
//15. O(n) similar to #14
public class Solution6
{
    public int removeDuplicates(int[] nums)
    {
        int dupes = 0;

        for (int i = 1; i < nums.length; i++)
        {
            if (nums[i] == nums[i - 1])
                dupes++;

            nums[i - dupes] = nums[i];
        }

        return nums.length - dupes;
    }
}