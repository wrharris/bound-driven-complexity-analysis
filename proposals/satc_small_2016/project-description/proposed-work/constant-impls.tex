% talk about automatically synthesizing side-channel free
% implementations
\subsubsection{Synthesizing side-channel free implementations}
\label{sec:constant-impls}
% this is a problem in practice:
Practical applications can leak critical information about their
sensitive inputs over channels other than their output values, such as
the time that they spend to perform a computation or their effect on
the system cache due to the addresses that they access;
%
such channels of information are referred to collectively as
\emph{side channels}.
%
In particular, an attacker who can observe the execution time of
natural implementations of cryptographic algorithms, including
Diffie-Helman, RSA, and DSS, can learn sufficient information about
the secret keys given to them to significantly reduce the security
guarantees provided by such algorithms~\cite{kocher96}.
%
Moreover, even an attacker with only remote access to a target system
over a network can learn sufficient information about the secret keys
used by OpenSSL to attack it successfully~\cite{brumley03}.
%
An attacker can access such channels with particular effectiveness on
cloud systems~\cite{ristenpart09,zhang14}.

% code: copying a subarray
\begin{figure*}
  \centering
  \begin{floatrow}
    % include gzip code:
    \ffigbox[0.48\textwidth]{ \input{cpy-subarray.c} }
    %
    {\caption{\cc{copy_subarray}: copies the subarray of \cc{in} at
        index \cc{l_idx} of length \cc{sub_len} to \cc{out}.}
      \label{fig:cpy-subarray} }

    % include policy:
    \ffigbox[0.48\textwidth]{\input{cpy-subarray-const.c} }
    { \caption{\cc{ct_copy_subarray}: functionally equivalent to
        \cc{copy_subarray}, but side-channel free.} 
      \label{fig:cpy-subarray-const} }
  \end{floatrow}
\end{figure*}

% introduce the programming problem:
Transforming program modules so that they do not reveal secret
information over side channels is a hard programming problem.
%
The problem stems from the fact that a programmer must determine if
all \emph{pairs} of runs that start from inputs with equivalent secret
inputs take the same amount of time to execute.
%
\autoref{fig:cpy-subarray} contains a procedure \cc{copy_subarray},
originally presented in previous work on verifying the absence of side
channels~\cite{almeida16}, that copies a subarray of a given input
buffer \cc{in} to a destination buffer \cc{out}.
%
\cc{copy_subarray} contains a timing channel that leaks information
about \cc{l_idx}.
%
In particular, \cc{copy_subarray} loops over the entire array stored
in \cc{in} (lines 4---7).
%
For each index \cc{i} of \cc{in}, \cc{copy_subarray} performs
operations that take a different amount of time depending on if \cc{i}
is greater than \cc{l_idx}.
%
An attacker who knows the amount of time taken by each operation and
who can observe the amount of time taken by \cc{copy_subarray} can
thus learn the value of \cc{l_idx}.
%
An attacker who can observe the addresses accessed by
\cc{copy_subarray} (possibly indirectly by observing addresses bound
in a system cache shared with \cc{copy_subarray}) over a run can
similarly learn information about \cc{l_idx}.

% introduce side-channel free implementation as soln:
\autoref{fig:cpy-subarray-const} contains an implementation of a
procedure \cc{ct_copy_subarray} that is functionally equivalent to
\cc{copy_subarray}, but does not leak information about \cc{l_idx}
over a timing channel or the addresses that it accesses.
%
Although \cc{ct_copy_subarray} iterates over some of the elements in
\cc{in} (lines 10---12), it first initializes the values stored in
\cc{out} to be $0$ by iterating over a sufficiently large prefix (line
9).
%
For each element in the input subarray of \cc{in},
\cc{ct_copy_subarray} performs a nested iteration over \cc{out} that
ensures that the memory addresses accessed by \cc{ct_copy_subarray} do
not reveal sensitive information about its inputs.

% our proposed work, result:
We propose to use \sys to generate a synthesizer that automatically
synthesizes side-channel free implementations.
%
Such a synthesizer will take as input a reference program $P$ and a
set of predicates $\mathcal{Q}$ over the inputs of $P$ that must not
be revealed to an attacker who can observe the execution time of a
program.
%
The synthesizer will produce a program that is functionally equivalent
to $P$, but does not reveal any of the predicates $\mathcal{Q}$ over
side channels.

% our proposed work: challenges
In order to generate such a synthesizer, we will have to address
several problems not addressed in previous work.
%
In particular, previous work on verifying that a given implementation
is constant-time~\cite{almeida16} establishes that in order to prove
that an implementation is constant time, one must establish the
validity of relational invariants that prove that all pairs of runs
over equivalent inputs execute in the same amount of time.
%
Moreover, in order to \emph{synthesize} a functionally-correct
constant-time implementation, the synthesizer must establish
\emph{two} collections of relational invariants.
%
In an addition to the relational invariants over pairs of runs that
establish that the synthesized program is constant-time, the
synthesizer must synthesize a \emph{simulation relation} (see
\autoref{sec:unify}) from the reference implementation to the
synthesized program that establish that the reference and synthesized
programs are functionally equivalent.
%
Previous work on syntax-guided synthesis~\cite{alur13} does not
provide techniques for synthesizing such a program, accompanied by a
simulation relation.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../../p"
%%% End: 
