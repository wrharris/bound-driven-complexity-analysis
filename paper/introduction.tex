% Introduction: a bunch of fluff
\section{Introduction}
\label{sec:introduction}
% Background.
In many contexts, programmers must be able to derive strong guarantees
about the performance of their program.
%
Such contexts can include both mission-critical systems and
high-performance code that executes on servers.
%
If such programs fail to satisfy resource bounds that a programmer
expects, there can be dire consequences for both the performance, and
in some cases security~\cite{apache-killer}, of the application and
its system.
%
Providing programming tools that enable programmers to understand when
and why their programs use resources as expected is a critical open
problem.

% Previous work: eager.
Previous work has introduced bound \emph{analyses}, which take a
program $P$ and always infer a sound bound on the resources used by
$P$~\cite{albert12,alias10,gulwani09a,gulwani09b,gulwani10,sinn14}.
%
The key contribution of bound analyses is that they can often provide
useful information to a programmer with no additional effort required
of the programmer.
%
However, the limitation of bound analyses is that, by the nature of
the problem that they address, they cannot ensure that the bound that
they infer is the tightest one possible, and cannot be used to show
that a program does \emph{not} satisfy an expected bound.

% Us: bound-driven complexity analysis.
In this work, we introduce a bound-driven automatic complexity
\emph{verifier}, named \sys.
%
\sys takes from a programmer a program $P$ \emph{and an expected
  bound} $B$ on the number of steps of execution that $P$ takes in
terms of its inputs.
%
\sys synthesizes either (1) a proof that $P$ satisfies $B$ on all
inputs or (2) a run of $P$ on which does not satisfy $B$ (\sys may
also fail to terminate).
%
While \sys thus requires a programmer to spend additional effort to
construct an expected bound $B$, when successful, it always gives the
programmer a concrete guarantee concerning the performance of the
program with respect to $B$.

% Techniques to address challenge: a safety model checker
\sys casts the problem of verifying that a given program $P$ satisfies
a given bound $B$ as verifying that $P$ satisfies a safety property
that specifies that a cost counter satisfies a potentially-non-linear
constraint over values in the program's initial state.
%
Like conventional automatic safety verifiers, \sys attempts to infer
bounds on a counter in terms of state variables by selecting
individual paths of execution of $P$, proving that all runs of each
individual path satisfy $B$, and combining the proofs for each path to
prove that all runs of $P$ satisfy $B$.

% Challenges.
However, developing a complexity verifier presents key technical
challenges that are not addressed when designing an eager bounds
analysis or verifiers for classes of safety properties.
%
In particular, invariants that are used to express proofs of bound
satisfaction typically must be expressed in an undecidable theory,
such as the theory of non-linear arithmetic.
%
As a result, conventional techniques for automatically verifying
safety properties~\cite{ball01,henzinger02,henzinger04,mcmillan06},
which rely on automatic decision procedures to infer invariants that
prove the safety of all runs of a single path, cannot be applied
directly.
%
Second, inductive invariants used in proofs of bound satisfaction
typically are defined over non-linear terms that are not apparent from
the original program or its semantic constraints.

% Core technical contribution:
The core technical contribution of \sys is its technique for
constructing a proof that runs of a program path satisfy a given
non-linear bound.
%
To do so, \sys uses a novel theorem prover for non-linear arithmetic,
which itself uses a decision procedure for the combination of theories
of linear arithmetic and uninterpreted functions.
%
To determine satisfiability of a path formula and synthesize
invariants for all runs of the path in a non-linear theory, the
theorem prover lazily refines an approximation of the theory of
non-linear arithmetic.
%
To do so, the theorem prover iteratively attempts to find a model of
the path formula under the prover's maintained approximation of
non-linear arithmetic.
%
If it finds a model, it tests the model as a model of the path formula
under the standard model of non-linear arithmetic.
%
If the model under the approximation diverges from the standard model
such that the divergence changes the final evaluation of the path
formula, then the prover uses the divergence to guide a search for
quantifier-free theorems of non-linear arithmetic that are sufficient
to prevent similar divergences.

% Results:
Our key results are that \sys is sound, and in practice, it can
potentially be applied by programmers to automatically synthesize
either proofs that an implementation of a subtle algorithm satisfies
an expected bound or a run of the implementation that does not satisfy
the bound.
%
In particular, we implemented \sys as a verifier for JVM bytecode
programs, and ran it to verify expected complexity bounds of programs
submitted by students as solutions to challenge problems hosted on
several online coding platforms~\cite{codechef,leetcode,codeforces}.
%
We have used \sys to prove or disprove that over 20 programs satisfy
or not satisfy complexity bounds.

% Paper outline.
The remainder of this paper is organized as follows.
% Overview:
\autoref{sec:overview} gives an informal overview of \sys by example.
% Background:
\autoref{sec:background} reviews previous work on which \sys is based.
% Problem and approach:
\autoref{sec:approach} describes \sys in technical detail.
% Evaluation:
\autoref{sec:evaluation} gives an empirical evaluation of \sys.
% Related work:
\autoref{sec:related-work} compares \sys to related work on complexity
analysis.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "p"
%%% End:
