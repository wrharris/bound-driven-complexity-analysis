public static void Scroll(int t, int n, int a, int b)
  throws IOException {
  BufferedReader br= 
    new BufferedReader(new InputStreamReader(System.in));
  int i,j,k;
  for(i = 0; i < t;i++) {      
    j = 0;
    while(a > 0) {
      if(a % 2 == 1) j++;
      a = a / 2; }
    k = 0;
    while(b > 0) {
      if(b % 2 == 1) k++;
      b = b / 2; }
    if(j + k == n)
      System.out.println((int)(Math.pow(2, n) - 1));
    else if(j + k < n) 
      System.out.println((int)(Math.pow(2, n) -
                               Math.pow(2, n - (j + k))));
    else 
      System.out.println((int)(Math.pow(2, n) -
                               Math.pow(2, j + k - n))); } }
