 // 1. O(n) - simple algo finding max profit/ can be used with grades or a combination of this and one of the below algos
public class Solution8 {
    public int maxProfit(int[] prices) {
        if (prices.length == 0)
        {
            return 0;
        }
        int max = 0, min = prices[0];
        int profit = 0;

        for (int i = 1; i < prices.length; i++)
        {
            if (prices[i] < min)
            {

                min = prices[i];
            }
            else
            {
                if (prices[i] - min > profit)
                {
                    profit = prices[i] - min;
                }
            }
         }
     return profit;

    }
}













 /