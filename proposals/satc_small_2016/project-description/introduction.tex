% introduction:
\section{Introduction}
\label{sec:introduction}
%
Denial of Service (DoS) vulnerabilities continue to undermine the
reliability of critical systems~\cite{perlroth16}.
%
In general, DoS vulnerabilties in a system can stem from complex
combinations of flaws at multiple levels of a system, including
structural issues in the system's network topology and network
protocols.
%
However, they also often stem unexpected behaviors in the
implementations of critical systems.

% existing work:
While research programming languages and software verifiers have
primarily focused on enabling developers to build systems that do not
perform faults, recent work has proposed languages and verifiers that
attempt to enable programmers to ensure that their program satisfies
an expected bound on the amount of resources that it consumes.
%
Such approaches typically attempt to verify that a program satisfies
an expected bound on the amount of time that is
uses~\cite{srikanth17}, are used to check functional
programs~\cite{hoffman10,hoffman11}, certify that a program uses a
bounded amount of stack space~\cite{carbonneaux14}, or rely on manual
verification~\cite{carbonneaux15}.

% problems not addressed by existing work:
Unfortunately, such approaches cannot be directly applied to
automatically verify that imperative programs that operate on shared,
linked data structures do not exhaust dynamically-allocated memory.
%
There are extensive examples of such memory-exhaustion vulnerabilities
in such programs, including the first publicly-disclosed DoS
vulnerability in Chrome~\cite{cve-2008-4340},
%
the Apache web server~\cite{cve-2008-1700},
%
vulnerabilities in previous versions of Microsoft's operating
systems~\cite{cve-2009-1928}, mail servers~\cite{cve-2006-1173},
communication software~\cite{cve-2009-2726},
Office~\cite{cve-2008-5180}, web application
firewalls~\cite{cve-2009-2299}, and the Opera
browser~\cite{cve-2009-2540}.
% context: system-resource exhaustion:
An attacker can cause a program to exhaust not only available memory
but other critical resources provided by the program's host system,
such as file descriptors and network connections.
%
There are extensive examples of such vulnerabilties in critical
systems as well, HTTP servers~\cite{slowloris},
%
communications programs developed by
Cisco~\cite{cve-2009-2054,cve-2009-2874}, the ClamAV virus
scanner~\cite{cve-2007-0897}.

% discuss existing work on memory leaks:
Significant previous
works~\cite{bond08,norvark09,qin05,sagiv02,valgrind} has focused on
detecting low-level memory consumption errors that are independent of
intended functionality, namely memory leaks.
%
Unfortunately, such approaches are not well-suited to determining if a
program is free of memory-exhaustion vulnerabilities for multiple
reasons.
%
A program leaks memory if along some run, it allocates a memory cell
that is eventually not reachable from any of its variables (i.e., not
\emph{live}).
%
However, many vulnerable programs, including all of the programs
discussed above, do not leak memory, but instead maintain a
prohibitively large set of memory cells that are all reachable from
program variables.

% proposed work:
We propose to develop automatic verifiers that can be used by a
developer to prove or disprove that a given program module satisfies
an expected bound on the amount of memory or system resources that it
uses.
%
We will develop such verifiers each in three phases.
%
In the first phase, we will extend \emph{shape logics}---such as
separation logic~\cite{calcagano11,distefano06,reynolds02} and
reachability
logics~\cite{itzhaky13,itzhaky-ba14,itzhaky-bj14}---proposed recently
for describing structural properties of program heaps, such as
disjointness of data structures or list acyclicity, to describe
\emph{quantitative} properties of heaps, such as the property that one
list maintained in the heap is bounded by the size of another list.

% second phase: deductive verification
In the second phase, we will develop \emph{automatic validators} that
take programs annotated with candidate invariants in the logics
designed in the first phase along with a resource-usage bound
expressed in the logics and determine if the invariants are valid and
prove that the program indeed satisfies the bound.
%
The key technical challenge in developing such validators will be in
developing automatic decision procedures for the logics designed in
the first phase.
%
We will develop such decision procedure by combining the decision
procedure that we developed in previous work~\cite{srikanth17} with
recent work on developing decision procedures for shape
logics~\cite{brotherston14,itzhaky13,itzhaky-ba14,itzhaky-bj14,seshia03}.

% third phase: automatic verification
In the third phase, we will develop \emph{automatic verifiers} that
take a program and only a resource-usage bound expressed in the logic
and automatically infer valid invariants in the logic that prove that
a program satisfies a bound, or generate executions on which it fails
to satisfy the bound.
%
The key technical challenge in developing such verifiers will be in
developing developing \emph{interpolating theorem provers} for each
logic that we propose.
%
Such theorem provers are used to infer invariants by automatic
verifiers for safety properties~\cite{mcmillan06,heizmann10}.
%
To develop each interpolating theorem prover, we will combine previous
work on developing interpolating theorem provers for shape
logics~\cite{albarghouthi15,drews16} with our previous work on
developing an interpolating theorem prover for logics used to count
execution steps taken by a program~\cite{srikanth17}.

% evaluation
We will evaluate the verifiers that we develop on program modules
collected both from critical systems.
%
We will use the benchmarks to evaluate to what extent our verifier can
provide developers with both (1) bugs that explain when their
implementations use a prohibitively large amount of memory and (2)
proofs that explain why their programs will always use an acceptably
low amount of memory.
% education
We will also evaluate the verifiers that we develop, particularly for
verifying freedom from memory exhaustion, as automatic aids for
teaching programmers to implement space-efficient algorithms.
%
In particular, we will apply our verifier to the repository of public
programming solutions collected in previous work~\cite{srikanth17}.

% broader impact:
The impact of the proposed work will be broad.
%
Over the course of the project, we will use the developed verifiers to
prove or disprove freedom from memory and system-resource exhaustion
for critical modules of mobile platforms and critical systems that
interface with untrusted users.
%
In addition to evaluating the verifiers that we develop, we will
develop the first shared repository of verifiers for memory exhaustion
and system-resource exhaustion.
%
We will also use our verifier to develop automatic programming
education aids, which will enable developers to effectively develop
and validate resource-efficient implementations.
%
We will incorporate the results of the research in modules of courses
in software security, verification, and programming languages that we
have designed continue to teach.
%
We will use the projects to continue to introduce women students and
underrepresented minorities to research.

% work plan:
The proposed work would be performed by the PI and two PhD students
over three years.

% outline remainder of paper:
The remainder of this proposal is organized as follows.
%
In \autoref{sec:previous-work}, we review previous work that we have
performed on automatically verifying that a program satisfies an
expected concrete bound on execution time.
%
In \autoref{sec:proposed-work}, we propose work on automatically
verifying freedom from data-dependent resource exhaustion attacks to
be performed over the course of this project.
%
In \autoref{sec:broader-impact}, we describe broader impacts of the
proposed work.
%
In \autoref{sec:prior-results}, we review results from previous
NSF-funded projects.
%
In \autoref{sec:work-plan}, we propose a phased plan over which the
proposed work will be performed.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "p"
%%% End: 
