% exprog: the running example
\subsection{An iterative implementation of binary search}
\label{sec:binary-search}
% introduce the program:
\autoref{fig:binary-search} contains source code for an implementation
of binary search adapted from a class named \cc{BinarySearch} posted
to the LeetCode online coding platform;
%
the hosted code has been simplified and refactored for the purpose of
illustrating our approach, but \sys verifies \cc{BinarySearch} without
manual modifications.

% program functionality, line by line:
\cc{BinarySearch} takes an array of integers \cc{arr} and an integer
\cc{n}, and if \cc{arr} stores the value \cc{n}.
%
If \cc{arr} is sorted and contains \cc{n}, then \cc{BinarySearch}
returns an index at which it stores \cc{n}.
%
\cc{BinarySearch} executes a loop that maintains the inductive
invariant that, under the above assumptions, there is some index of
\cc{arr} that is greater than or equal to \cc{fst} and less than
or equal to \cc{mid}.
%
Before \cc{BinarySearch} executes the loop, it establishes the
invariant by initializing \cc{fst} to $0$ (line 2) and initializing
\cc{lst} to $\cc{len}(\cc{arr}) - 1$ (line 3).

% walk through each iteration:
In each iteration of the loop, \cc{BinarySearch} tests that $\cc{fst}
\leq \cc{lst}$ (line 4).
%
If so, \cc{BinarySearch} computes the average of \cc{fst} and \cc{lst}
and stores the result in \cc{mid} (line 5).
%
\cc{BinarySearch} then tests if the value of \cc{arr} at index
\cc{mid} is less than \cc{n} (line 6);
%
if so, \cc{BinarySearch} updates \cc{fst} to store \cc{mid}
incremented by $1$ (line 7).
%
Otherwise, \cc{BinarySearch} tests if the value of \cc{arr} at
\cc{mid} is equal to \cc{n} (line 8);
%
if so, \cc{BinarySearch} immediately exits the loop (line 9).
%
Otherwise, \cc{BinarySearch} updates \cc{lst} to store the index in
\cc{mid} decremented by $1$ (line 10) and completes the current loop
iteration.

% talk about inductive invariants
\subsection{Automatic validation of step-count invariants}
\label{sec:campy-validation}
%
Each run of \cc{BinarySearch} uses an amount of time bounded by a
logarithmic function (base 2) of the length of \cc{arr}.
%
We can express such a relationship as a property of the state of
\cc{BinarySearch} by interpreting \cc{BinarySearch} under a semantics
that extends the state of \cc{BinarySearch} with a variable \cc{cost}
that models the current cost of an execution.
%
Under the extended semantics, \cc{cost} is incremented whenever
\cc{BinarySearch} performs a complete iteration of a loop (i.e.,
whenever \cc{BinarySearch} steps from line 9 to line 8 in
\autoref{fig:binary-search}).
%
Proving that \cc{BinarySearch} executes in time bounded by
$\log{\length(\cc{arr})} + 1$ can be expressed as requiring all runs
of \cc{BinarySearch} to satisfy the post-condition $\binsearchbound
\equiv \mathsf{cost} \leq \log{(\length(\cc{arr}))} + 1$.

% punt on non-terminating executions:
In general, under such a formulation of bound satisfaction, a program
with non-terminating executions could potentially satisfy a bound.
%
However, the formulation can easily be adapted to require that the
condition on $\mathsf{cost}$ be satisfied at each cutpoint in the
program.
%
To simplify the presentation of \sys, in this paper we only explicitly
consider the problem of verifying that a program satisfies a
step-count bound on all terminating executions, and thus that a
step-count condition need only be checked at the final location of
the program.

% proof:
\cc{BinarySearch} can be proved to satisfy $\binsearchbound$ by
establishing the loop invariant $I \equiv \mathsf{cost} + 1 +
\log{(\cc{lst} - \cc{fst})} \leq \log{\cc{len}(\cc{arr})}$ and proving
that $I$ implies that $\binsearchbound$ is satisfied at the end of
execution.
%
Using the rules of Hoare logic, such a problem can be reduced to
discharging entailments that formulate that $I$ is entailed by the
initial condition on program state and is preserved by each path
through the loop.
% inductive, path 7:
E.g., the condition that each state that satisfies $I$ and steps
through the loop on the path that contains line 7 must step to a state
that satisfies $I$ can be formulated as the entailment:
%
\begin{align}
  \mathsf{cost} + \log{(\cc{lst} - \cc{fst})} \leq 
  \log{(\cc{len}(\cc{arr}))} + 1 \entails & \label{eqn:ind-0} \\ %
  \mathsf{cost} + %
  \log{(\cc{lst} - (\cc{fst} + \cc{lst}) / 2)} \leq %
  \log{(\cc{len}(\cc{arr}))} + 1 \notag
\end{align}

% axioms for inductive entailments:
\autoref{eqn:ind-0} can be proved using the axioms of \euflia and the
quantifier-free non-linear theorems
%
\begin{align}
  \log{2} & = 1 \label{thm:ax-log-2} \\
  \log{((\cc{lst} - \cc{fst}) / 2)} & = %
  \log{(\cc{lst} - \cc{fst})} - \log{2} \label{thm:ax-log-sub}
\end{align}
%
\autoref{thm:ax-log-sub} is the theorem $\forall x, y.\ \log{x / y} =
\log{x} - \log{y}$ grounded on terms $\cc{lst} - \cc{fst}$ and $2$.

% talk about what sys does:
\sys, given a program such as \cc{BinarySearch} and the step-count
invariant $I$, can automatically verify that \cc{BinarySearch}
executes in time $\log{\cc{n}}$.
%
Verifying that \cc{BinarySearch} satisfies such a bound, even given
candidate invariants, is challenging because the bound and invariant
requires \sys to determine the validity of formulas expressed in
a non-linear theory.
%
Determining satisfiability of formulas in such theories is difficult
for state-of-the-art decision procedures.
%
\sys includes an implementation of a novel theorem prover that
attempts to determine satisfiability of formulas in such theories by
selectively introducing groundings of arithmetic theorems, such as
\autoref{thm:ax-log-2} and \autoref{thm:ax-log-sub}.
%
The operation of the theorem prover is illustrated in more detail in
\autoref{sec:campy-verification}.

% synthesizing invariants:
\subsection{Automatic verification of step-bound satisfaction}
\label{sec:campy-verification}
%
In this section, we illustrate how \sys automatically synthesizes
inductive step-count invariants that prove that a given program $P$
satisfies a given resource bound $B$.
%
E.g., if \sys is given program \cc{BinarySearch} and expected bound
$\binsearchbound$, \sys attempts to prove bound satisfaction by
synthesizing inductive step-count invariants similar to those given in
\autoref{sec:campy-validation}.

% give proof of bound satisfaction from an individual path
\begin{figure*}
  \centering
  \includegraphics[width=.98\linewidth]{fig/bin-search-path.pdf}
  % 
  \caption{A control path $q$ of \cc{BinarySearch}.
    %
    Each node $n$ depicts a point in $q$ and is annotated with its
    control location;
    %
    points with path critical invariants are also annotated with their
    path invariant.
    %
    Each edge from $n$ to $n'$ is annotated with the instructions of
    \cc{BinarySearch} taken when \cc{BinarySearch} steps from the
    location of $n$ to the location of $n'$.
    %
  }
  \label{fig:bin-search-path}
\end{figure*}
% introduce notion of path-based verification
Similarly to conventional automatic verifiers for safety
properties~\cite{henzinger02,henzinger04,mcmillan06}, \sys attempts to
synthesize inductive resource invariants for $P$ by iteratively
selecting a control path $q$ of $P$, synthesizing path step-count
invariants that prove that all runs of $q$ satisfy $B$, and combining
the step-count invariants for individual paths to attempt to
search for inductive invariants of $P$.
%
E.g., if \sys is run on \cc{BinarySearch} and bound $\binsearchbound$,
it may select the control path of \cc{BinarySearch} that iterates
through the loop in \autoref{fig:binary-search} twice, depicted in
\autoref{fig:bin-search-path}.
%
\sys would synthesize invariants for each point in $q$, such as the
invariants given in \autoref{fig:bin-search-path} for each point in
$q$.

% how we do it:
The key technical problem addressed in this work is, given a resource
bound $B$ expressed in a non-linear theory and a control path $q$, to
automatically synthesize path invariants for $q$ that are sufficient
to prove that all runs of $q$ satisfy $B$.
%
While a similar problem is addressed by automatic verifiers for safety
properties, the key distinction between synthesizing path invariants
that support a safety property and synthesizing path invariants that
support a step-count bound is that step-count bounds are often
expressed in non-linear theories.
%
E.g., the bound $\binsearchbound$ for \cc{BinarySearch} includes a
term constructed from the $\log$ function.

% key observation:
\sys synthesizes sufficient path invariants on resource usage
expressed in a non-linear theory $\mathcal{T}$ accompanied by a
standard model by using a novel interpolating theorem prover, named
$\tpt$.
%
There cannot be a complete theorem prover for logics used to express
practical complexity bounds due to fundamental results establishing
the undecidability of non-linear arithmetic~\cite{godel34}.
%
However, the design of $\tpt$ is motivated by the key observation that
often, path invariants that prove that all runs of a path satisfy a
bound are supported by the axioms of a decidable theory used to
express the semantics of program instructions, combined with a small
set quantifier-free theorems of the non-linear theory over small
terms.
%
E.g., the validity of the path invariants for path $q$ of
\cc{BinarySearch} given in \autoref{sec:campy-verification} is
supported by \euflia extended with only the quantifier-free non-linear
theorems \autoref{thm:ax-log-2} and \autoref{thm:ax-log-sub}.

% tpt: design, case: no models of path formula under current theory
$\tpt$ attempts to simultaneously synthesize valid path invariants and
quantifier-free $\mathcal{T}$ theorems $A$ that support their validity
using a counterexample-guided loop.
%
In each iteration of the loop, $\tpt$ queries a decision procedure for
\euflia, named $\eufliaissat$, to determine if a formula whose models
correspond to runs of $q$ that do not satisfy bound $B$ (i.e., the
\emph{path formula} for $q$ and $B$, denoted $\pathformula{q}{B}$) and
a maintained candidate set $A$ are mutually unsatisfiable as \euflia
formulas.
%
If $\eufliaissat$ determines that $\pathformula{q}{B} \land A$ is
unsatisfiable as an \euflia formula, then $\tpt$ generates path
invariants for $q$ by running an \emph{interpolating theorem prover}
for \euflia on $\pathformula{q}{B} \land B$~\cite{mcmillan04}.

% example: path formula of q, bin-search bound:
E.g., for bound $\binsearchbound$ and path $q$ of \cc{BinarySearch}
(\autoref{sec:campy-validation}), the path formula
$\pathformula{q}{B}$ is
%
\begin{align*}
  % constraints for initial sequence:
  \mathsf{cost}_0 = 0 \land 
  \cc{fst}_0 = 0 \land %
  \cc{lst}_0 = \cc{len}(\cc{arr}) - 1 \land 
  % constraints for guard:
  \cc{fst}_0 \leq \cc{lst}_0 & \land \\ 
  % constraints for iteration 0:
  \cc{mid}_0 = (\cc{fst}_0 + \cc{lst}_0) / 2 \land %
  \cc{arr}[ \cc{mid}_0 ] < \cc{n} \land %
  \cc{fst}_1 = \cc{mid}_0 + 1 & \land \\
  \mathsf{cost}_1 = \mathsf{cost}_0 + 1 \land 
  % constraints for iteration 1:
  \cc{mid}_1 = (\cc{fst}_1 + \cc{lst}_0) / 2 & \land \\ %
  \cc{arr}[ \cc{mid}_0 ] \not< \cc{n} \land %
  \cc{arr}[ \cc{mid}_0 ] = \cc{n} \land %
  \mathsf{cost}_1 \not\leq \log{(\cc{len}(\cc{arr}))} + 1 & 
\end{align*}
%
While $\pathformula{q}{B}$ does not have a model in non-linear
arithmetic, it does have one in \euflia, where the function symbol
\cc{log} is uninterpreted.
%
In particular, one satisfying partial model is the following
assignment $m$:
% counter-model:
\begin{align*}
  % parameters:
  \mathbf{m(\log{4})} & \mathbf{= 0} &
  m(\cc{arr}[ 1 ]) & = 10 &
  m(\cc{arr}[ 2 ]) & = 20 \\
  m(\cc{len}(\cc{arr})) & = 4 & %
  m(\cc{n}) & = 20 & 
  % loop initialization:
  m(\mathsf{cost}_0) & = 0 \\
  m(\cc{fst}_0) & = 0 & %
  m(\cc{lst}_0) & = 3 &
  % iteration 0:
  m(\cc{mid}_0) & = 1 \\
  m(\cc{fst}_1) & = 3 & 
  m(\mathsf{cost}_1) & = 1 &
  % iteration 1:
  m(\cc{mid}_1) & = 2 
\end{align*}
%
$m$ is a model of $\pathformula{q}{B}$ under \euflia but not under
non-linear arithmetic because it assigns $\log{4}$ to $0$ (the
assignment is emphasized above in bold typeface).

% theorem prover case: model:
If $\eufliaissat$ returns an \euflia model $m$ of
$\pathformula{q}{B}$, then $\tpt$ evaluates $\pathformula{q}{B}$ on
$m$ using the standard model of all $\mathcal{T}$ functions.
%
If $\pathformula{q}{B}$ is satisfied by $m$ using the standard model
of $\mathcal{T}$, then $\tpt$ determines that $\pathformula{q}{B}$ is
satisfiable as a $\mathcal{T}$ formula, and as a result, that $q$ does
not satisfy the $\mathcal{T}$ bound $B$.
%
E.g., for path $q$ of \cc{BinarySearch} and bound $\binsearchbound$,
if $\tpt$ runs $\eufliaissat$ on $\pathformula{q}{\binsearchbound}$
and obtains model $m$, then it evaluates $\pathformula{q}{B}$ under
the model $m'$ that binds $\mathsf{cost}_0$, $\cc{fst}_0$, $\cc{len}$,
$\cc{lst}_0$, $\cc{mid}_0$, $\cc{arr}[\cc{mid}]$, and
$\mathsf{cost}_1$ to their values in $m$, and interprets $\log$ under
the standard model.
%
$m'$ does not satisfy $\pathformula{q}{B}$.

% walk through grounding axioms:
If $m'$ does not satisfy $\pathformula{q}{B}$, then $\tpt$ chooses a
clause $C$ of $\pathformula{q}{B}$ not satisfied by $m'$ and generates
a set of $\mathcal{T}$ formulas that are not satisfied by any model
that evaluates the $\mathcal{T}$ terms in $C$ to their values under
$m$.
%
E.g., when $\tpt$ attempts to find path invariants of path $q$ of
\cc{BinarySearch},, $\tpt$ determines that $m'$ does not satisfy the
clause $\mathsf{cost}_2 \leq \log{\cc{len}(\cc{arr})}$ of
$\pathformula{q}{\binsearchbound}$.
%
$\tpt$ enumerates theorems of non-linear arithmetic over the variables
$\mathsf{cost}_0$, $\cc{fst}_0$, $\cc{len}$, $\cc{lst}_0$,
$\cc{mid}_0$, $\cc{arr}[\cc{mid}]$, and $\mathsf{cost}_1$ until it
finds a minimal set that has no model $m'$ under which
$m'(\cc{len}(\cc{arr})) = m(\cc{len}(\cc{arr})) = 8$ and $m'(\log)(8)
\not= 3$.
%
One such set are the axioms of non-linear arithmetic
\autoref{thm:ax-log-2} and \autoref{thm:ax-log-sub} defined over
logical variables that model distinct definitions of \cc{fst} and
\cc{lst} that occur in $q$.
%
I.e., the following set of theorems is minimal and sufficient:
%
\begin{align}
  % ax-log-2:
  \log{2} = & 1 \label{thm:ax-log-2-q} \\
  % ax-log-sub: init:
  \log{((\cc{lst}_0 - \cc{fst}_0) / 2)} = & %
  \log{(\cc{lst}_0 - \cc{fst}_0)} - \log{2} \label{thm:ax-log-sub0} \\
  % ax-log-sub: one iter:
  \log{((\cc{lst}_0 - \cc{fst}_1) / 2)} = & %
  \log{(\cc{lst}_0 - \cc{fst}_1)} - \log{2} \label{thm:ax-log-sub1} \\
  % ax-log-sub: two iters:
  \log{((\cc{lst}_1 - \cc{fst}_1) / 2)} = & %
  \log{(\cc{lst}_1 - \cc{fst}_1)} - \log{2} \label{thm:ax-log-sub2}
\end{align}

% look ahead to the end game:
$\tpt$ iteratively generates models and enumerates theorems until
either it finds a $\mathcal{T}$ model of $\pathformula{q}{B}$ or it
finds a set of quantifier-free $\mathcal{T}$ theorems $A$ such that
$\pathformula{q}{B} \land A$ is unsatisfiable.
%
E.g., when $\tpt$ attempts to find path invariants of path $q$ of
\cc{BinarySearch} that prove that $q$ satisfies $\binsearchbound$, if
$\tpt$ enumerates the theorems
\autoref{thm:ax-log-2-q}---\autoref{thm:ax-log-sub2}, then $\tpt$
determines in its next iteration that $\pathformula{q}{B} \land A$ is
an unsatisfiable \euflia formula.
%
$\tpt$ runs an interpolating theorem prover for \euflia on
$\pathformula{q}{B} \land A$ to obtain path invariants for $q$, such
as the path invariants given in \autoref{fig:bin-search-path}.

% head off caveats, limitations:
The effectiveness of $\tpt$ depends critically on its ability to
efficiently enumerate a set of quantifier-free theorems that is
sufficient to refute a model with an inaccurate interpretation of
theory functions, and ultimately to support valid path and inductive
invariants on resource usage.
%
E.g., $\tpt$, given path $q$ of \cc{BinarySearch}, could generate the
valid theorem $\log{4} = 2$ to prove that $q$ satisfies
$\binsearchbound$, but such a theorem could not be used to synthesize
inductive step-count invariants of \cc{BinarySearch} that prove that
all of its runs satisfy $\binsearchbound$.

% technique 0: grounding order:
To select general theorems, $\tpt$ enumerates theorems by grounding a
small set of universally-quantified axioms on quantifier-free terms,
first enumerating small terms that minimize the use of constants.
%
E.g., to prove that a given path satisfies a bound expressed in terms
of $\log$, it enumerates instances of the theorems
%
\begin{align*}
  \forall x.\ x \leq 0 \implies \log{x} & = 0 \\
  \forall x, y.\ \log{(x \cdot y)} & = \log{x} + \log{y} \\
  \forall x, y.\ \log{(x / y)} & = \log{x} - \log{y}
\end{align*}
%
with $x$ and $y$ grounded to small terms over the logical variables
used to model the state of $q$, such as $\cc{fst}_0$, $\cc{lst}_0$,
and $\cc{lst}_0 - \cc{fst}_0$.

% technique 1: restrict variable ordering:
To enumerate a sufficient set of theorems efficiently, $\tpt$ only
enumerates theorems over sets of logical variables that model state at
some common point in the given control path.
%
I.e., $\tpt$ uses the \emph{locality} of variable uses and definitions
along a path to guide theorem enumeration similarly to how safety
verifiers use the locality of uses and definitions to guide the
selection of path invariants~\cite{henzinger04,mcmillan06}.
%
E.g., when enumerating axioms for path $q$ of \cc{BinarySearch},
$\tpt$ enumerates theorems defined over logical variables $\cc{fst}_0$
and $\cc{lst}_0$ (which model state at points $b$ and $c$) or
$\cc{fst}_1$ and $\cc{lst}_0$ (which model state at points $d$ and
$e$), but not $\cc{fst}_0$ and $\cc{fst}_1$ or $\mathsf{cost}_0$ and
$\cc{fst}_1$.
%
The locality-guided enumeration strategy is given in detail in
a complete paper describing our work~\cite{srikanth17}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "p"
%%% End: 
