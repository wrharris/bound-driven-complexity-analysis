\section{Evaluation}
\label{sec:evaluation}
% Overview of questions.
We performed an empirical evaluation of \sys in order to answer the
following questions: \textbf{(1)} Is \sys expressive?
%
I.e., can it prove that programs that implement subtle algorithms
satisfy expected bounds, which in practice are often non-linear?
% efficiency
\textbf{(2)} Is \sys efficient?
%
I.e., when it proves that a program satisfies a bound or finds an
input which the program does not satisfy the bound, does it do so
efficiently enough to be potentially useful for a programmer?

% Overview of what we did to answer the questions.
To answer the above questions, we collected benchmarks from platforms
that host solutions to programming
exercises~\cite{codechef,leetcode,codeforces}.
%
For each benchmarks program $P$, we derived a bound that we expected
$P$ to satisfy and a bound that we did not expect $P$ to satisfy by
inspecting metadata associated with $P$, such as the $P$'s signature
and information on the site at which it was posted.
%
We implemented \sys as a tool that supports programs represented in
JVM bytecode, using the Soot~\cite{soot} analysis framework and
Z3~\cite{z3} interpolating theorem prover, and applied it to each
program paired with its expected bound and unexpected bound, and
observed if \sys found a proof of bound satisfaction or counterexample
run for each bound.

% experimental setup:
We performed all experiments on a machine with 16 1.4 GHz processors
and 132 GB of RAM.
%
The current implementation of \sys executes using a single thread.
% data plan:
\sys, along with a container replicating our experimental setup for
evaluation, are publicly available~\cite{campy}.
%
Each benchmark on which we evaluated \sys is publicly available, and
we are currently working with online coding platforms to potentially
release the programs as a benchmark suite for program analyses and
verifiers.

% overview of the results:
In summary, our evaluation answered the above questions positively.
%
\sys was able to verify that program satisfied or did not satisfy
expected bound in at most a few seconds.
% Give an overview of the rest of the evaluation section.
The rest of this section describes our results in detail.
%
In \autoref{sec:benchmarks}, we discuss a selection of solutions on
which we ran \sys.
%
In \autoref{sec:results}, we draw conclusions from our results on the
effectiveness of \sys.

% Library benchmarks:
\subsection{Benchmarks}
\label{sec:benchmarks}
%
In this section, we illustrate the operation of \sys on selected
benchmarks from the LeetCode~\cite{leetcode},
CodeChef~\cite{codechef}, and CodeForce~\cite{codeforces} coding
platforms.
%
For each benchmark, we used a cost model in which each backward branch
in a depth-first traversal of the control-flow graph costs a unit of
time and every other instruction incurs no cost.
%
The resulting bounds describe a relationship between a program's input
and the number of times that it executes an iterative computation.

% overview:
We will now illustrate the features and current limitations of \sys by
discussing three of the benchmark programs: \cc{lengthOfLIS},
\cc{SmartSieve}, and \cc{Cycle}.
%
\sys correctly verified the programs \cc{lengthOfLIS} and
\cc{SmartSieve}, but failed to prove or disprove that \cc{Cycle}
satisfied an expected bound.

% LIS:
\begin{figure}
  \input{code/LIS.java}
  \caption{\cc{lengthOfLIS}: a solution to the LIS Problem.
    %
  }
  \label{fig:LIS}
\end{figure}

% LIS: longest increasing subsequence:
\paragraph{Longest Increasing Subsequence}
% how the program works:
The Longest Increasing Subsequence (LIS) Problem is to take an array
of integers and return the length of a longest increasing subsequence
of the array~\cite{LIS}.
%
\cc{lengthOfLIS}, given in \autoref{fig:LIS}, was posted to LeetCode
as a solution to the LIS problem.
%
\cc{lengthOfLIS} iterates from $1$ to $n$, and at each intermediate
value $i$, maintains in array \cc{res} of the longest increasing
subsequence of \cc{nums} up to $i$, and maintains in \cc{len} the
length of the longest increasing subsequence (lines 7---18).
%
If the value in \cc{nums} at \cc{i} is greater than the current last
element in \cc{res}, then \cc{lengthOfLIS} extends \cc{res} to contain
$\cc{nums}[\cc{i}]$ (lines 9---11).
%
Otherwise, if the value in \cc{nums} at \cc{i} is less than or equal
to the last value in \cc{res}, then \cc{lengthOfLIS} updates the index
in \cc{i} and an entry of \cc{res} by performing a binary search over
the values in \cc{res} and \cc{nums} between \cc{i} and the length of
\cc{nums} (lines 12---18).

% proving a bound:
To derive a verified tight bound for \cc{lengthOfLIS}, we first
provided to \sys \cc{lengthOfLIS} and the bound $n^2$, derived from
identifying the nested loops of \cc{lengthOfLIS};
%
\sys verified that \cc{lengthOfLIS} satisfies the $n^2$ bound.
%
We then provided the bound $n \cdot \log{n}$, inspired by the
observation that the nested loop in \cc{lengthOfLIS} at lines 14---18
updates a value used in the loop guard with division by a constant;
%
\sys verified that \cc{lengthOfLIS} satisfies the bound $n \cdot
\log{n}$ as well.
%
To determine if \cc{lengthOfLIS} actually executes each loop in
constant time, we provided a bound of $n$ to \sys.
%
\sys returned an execution of \cc{lengthOfLIS} on which it does not
satisfy the bound $n$.

% SmartSieve:
\begin{figure}
  \input{code/SmartSieve.java}
  \caption{\cc{SmartSieve}: a solution to the Smart Sieve problem.
    %
  }
  \label{fig:SmartSieve}
\end{figure}

% SmartSieve:
\paragraph{SmartSieve}
% talk through the program:
The Smart Sieve Problem hosted on CodeChef is to take two integers
\cc{MAX} and \cc{k} and return an integer array containing the prime
factors of \cc{k} up to \cc{MAX}.
%
One solution submitted for the Smart Sieve Problem, \cc{SmartSieve},
is given in \autoref{fig:SmartSieve}.
%
\cc{SmartSieve} first stores the smallest prime factor of every even
number up to \cc{MAX} (namely, $2$) at its index in \cc{sp} and then
stores the smallest prime factor of every odd number up to \cc{MAX} at
its index in \cc{sp} (lines 2---12).
%
\cc{SmartSieve} then computes the prime factors of $k$ by iteratively
collecting the smallest prime of $k$ and then dividing $k$ by its
smallest prime (lines 15---17).

% bounds and sys:
It is somewhat immediate for an automatic verifier to determine that
\cc{SmartSieve} up to line 12 executes in $n^2$ steps, but less
immediate to prove that a complete run of \cc{SmartSieve} executes in
$n^2 + \log{n}$ steps.
%
Proving that \cc{SmartSieve} satisfies such a bound relies on proving
that the loop in lines \cc{15}---\cc{17} executes in $\log{n}$ steps.
%
\sys proves that \cc{SmartSieve} satisfies such a bound by
synthesizing the supporting invariant that at each index $k$,
$\cc{sp}[k] \geq 2$.

% Cycle of a graph (DFS failing example)
\begin{figure}
  \input{code/Cycle.java}
  \caption{\cc{Cycle}: a solution to the Cycle of a Graph Problem.
    %
  }
  \label{fig:Cycle}
\end{figure}

\paragraph{Cycle of a Graph}
% interface:
An instance of the Cycle of a Graph problem is an undirected graph $G$
consisting of $n$ nodes, where each node is connected to at least $k$
other nodes.
%
A solution is a cycle with at least $k + 1$ nodes.
%
\autoref{fig:Cycle} contains one solution to the problem given on
Codeforces.
%
\cc{Cycle} takes as input an integer \cc{v} storing
the node currently being traversed, %
\cc{x} the current recursion depth, %
\cc{k} the lower bound on cycle size, %
\cc{d} maintains the known depth of each node, %
\cc{a} store the list of nodes in the result cycle, and %
\cc{g} stores the adjacency list for all nodes in the graph.
%
\cc{Cycle} maintains a global variable \cc{done} that stores whether
or not it has found a cycle.
%
\cc{Cycle} outputs all nodes in the cycle to \cc{stdout}.

% walk through the code:
\cc{Cycle} first checks if it has already found a cycle (line 4).
%
If not, then \cc{Cycle} checks if the depth of \cc{v} in \cc{d} is
greater than $0$ (line 5).
%
If so, then \cc{Cycle} checks if the difference between the current
recursion depth \cc{x} and the depth of \cc{v} is greater than \cc{k}
(line 6).
%
If so, \cc{Cycle} prints the sequence of nodes visited between the
depth of \cc{v} and the current recursion depth (lines 7---8), stores
that it has found a cycle (line 9).
%
In either case, \cc{Cycle} returns (line 10).
% case:
If the depth of \cc{v} is not greater than $0$, then \cc{Cycle} stores
\cc{x} as the depth of \cc{v} (line 11).
%
For each successor \cc{cur} of \cc{v} (line 13), it stores \cc{cur} as
the next node visited (line 14) and recurses (line 15).

% result of bounds analysis:
When we ran \sys on \cc{Cycle} and the expected bound $n^2 \cdot k$,
it failed to prove that \cc{Cycle} satisfies the bound.
%
\sys failed to prove that \cc{Cycle} satisfied the bound because the
execution time of \cc{Cycle} is determined partly by it's traversal of
linked lists as values in an array, but \sys cannot use assumptions
about, or synthesize sophisticated invariants describing the size of
linked data structures.

% Results
\subsection{Results and Conclusions}
\label{sec:results}

% include experimental results
\input{results.tex}

The results of our evaluation are contained in \autoref{tbl:data}.
%
The names for most benchmarks given in \autoref{tbl:data} were derived
from information in the program source, such as the name of a class or
critical method.
%
In cases where a unique name was not apparent from the source, a name
of the form $\cc{Test}i$ was assigned.

% expressiveness:
Overall, our experience using \sys indicates that it can be applied to
verify that programs that implement intricate algorithms satisfy or do
not satisfy expected resource bounds.
%
As illustrated in \autoref{sec:benchmarks}, it was typically
straightforward to use an informal understanding of a program module
to derive a tight expected bound that \sys could verify.
%
Moreover, in cases where an expected bound was not apparent, we were
able to use both \sys's ability to verify that a program satisfies a
bound and to determine when it does not to find a reasonably tight
bound after a few uses.
%
Of the bounds listed in \autoref{tbl:data}, the bound for \cc{Jewel2}
contains a surprising coefficient of $350$.
%
This bound is immediate from the fact that \cc{Jewel2} contains loops
that iterate up to $150$ and $200$.
%
When run on the benchmark \cc{Checkpost}, \sys timed out as in the
case of \cc{Cycle}, because it was unable to infer sufficiently rich
step-counting invariants in terms of the dynamic data structure that
it allocated.

% performance:
In general, the performance of \sys strongly indicates that it could
be integrated into an automated educational aid for providing
immediate feedback to students about the efficiency of the programs.
%
Extending \sys to infer invariants over richer classes of data
structures could further improve its performance in practice.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "p"
%%% End:
