% Background:
\section{Background}
%
\label{sec:background}
%
In this section, we define a target language that we use to present
our approach (\autoref{sec:language}).
%
We then review previous foundations of formal logic
(\autoref{sec:logic}).
%
% give the language:
\subsection{Target Language}
\label{sec:language}
%
In this section, we define the structure (\autoref{sec:structure}) and
semantics (\autoref{sec:semantics}) of the language of programs that
\sys takes as input.

% Program structure: talk about program representation.
\subsubsection{Program Structure}
\label{sec:structure}
%
A program is a set of statements, each of which tests and updates
state, calls, or returns.
% Define spaces: locations, variables, instructions
Let $\procnms$ be a space of \emph{procedure names};
%
let $\brlocs$, $\calllocs$, and $\retlocs$ be disjoint sets of branch,
call, and return control locations, and let the set of all control
locations be denoted $\locs = \brlocs \union \calllocs \union
\retlocs$, with a distinguished \emph{initial location} \initloc and
\emph{final location} \finalloc.  Let $\mathsf{proc}: \locs \to
\procnms$ map each control location to the procedure that contains it,
with $\procof{\initloc} = \procof{\finalloc}$.
%
Let $\mathsf{entry}: \procnms \to \locs$ map each procedure to its
entry control location and $\mathsf{exit}: \procnms \to \locs$ map
each procedure to its exit control location.
%
The space of all variables is denoted $\vars$, and contains the
distinguished \emph{cost} variable $\cost \in \vars$.
%
The space of all instructions is denoted $\instrs$.
%
For each control location $\cc{L} \in \locs$ and sequence of locations
$s \in \locs^{*}$, $s$ is an \emph{immediate suffix} of $\cc{L} \cons
s$.

% define branch statement:
A program statement either tests and updates state, calls a procedure,
or returns from a call.
%
A pre-location, instruction, and branch-target-location is a
\emph{branch statement};
%
i.e., the space of branch statements is denoted $\brseqs = \brlocs
\times \instrs \times \locs$.
%
For each branch statement $b \in \brseqs$, the pre-location,
instruction, and post-location of $b$ are denoted $\prelocof{b}$,
$\instrof{b}$, and $\brtargetof{b}$, respectively.

% define call instructions:
A pre-location, call-target procedure name, and return-target control
location are a \emph{call statement};
%
i.e, the space of call statements is denoted $\callseqs = \calllocs
\times \procnms \times \locs$.
%
For each call statement $c \in \callseqs$, the pre-location, call
target, and return target of $c$ are denoted $\prelocof{c}$,
$\calltargetof{c}$, and $\rettargetof{c}$, respectively.
%
The call entry point of $c$ is denoted $\entryof{c} =
\entryof{\calltargetof{c}}$.

% define return statements:
A return location represents a \emph{return statement};
%
i.e., the space of return statements is denoted $\retseqs = \retlocs$.

% Collect over all sequences:
The space of all statements is denoted $\progseqs = \brseqs \union
\callseqs \union \retseqs$.
%
Each control location is the target of either potentially-many branch
statement or exactly one call statement.
%
For each call statement $c \in \callseqs$ and return statement $r \in
\retseqs$ such that $\calltargetof{c} = \procof{\prelocof{r}}$, $r$
\emph{returns to} $c$.
% Define program:
A program $P$ is a set of statements in which for each branch location
$\cc{L} \in \brlocs$ and location $\cc{L}' \in \locs$, there is at
most one branch statement, denoted $\brat{P}{\cc{L}}{\cc{L}'}$ with
$\prelocof{b} = \cc{L}$ and $\brtargetof{b} = \cc{L}'$.
%
The language of programs is denoted $\lang$.

% Program semantics: talk about the possible runs of a program.
\subsubsection{Program Semantics}
\label{sec:semantics}
% define runs. First, define paths
A run of a program $P$ is a sequence of stores that are valid along an
interprocedural path of $P$.
% Define nesting relations.
A nesting relation over indices models the matched calls and returns
of along a control path~\cite{alur09}.
%
For each $n \in \nats$, let the space of positive integers less than
$n$ be denoted $\ints_n$.
%
\begin{defn}
  \label{defn:nesting}
  For each $n \in \nats$ and $\neststo \subseteq \ints_n \times
  \ints_n$ such that for all indices $i_0, i_0', i_1, i_1' \in
  \ints_n$ with $i_0 \neststo i_0'$ and $i_1 \neststo i_1'$, either
  $i_0 < i_1 < i_1' < i_0'$, %
  $i_1 < i_1' < i_0 < i_0'$, or %
  $i_1 < i_0 < i_0' < i_1'$,
  %
  $\neststo$ is a \emph{nesting} relation over $n$.
\end{defn}
%
For each $n \in \nats$, the nesting relations over $\ints_n$ are
denoted $\nestings{n}$.
%
For each $i, j < n$ and nesting relation $\neststo \in \nestings{n}$,
we denote $(i, j) \in \neststo$ alternatively as $i \neststo j$.

% Define a path:
A control path is a sequence of control locations visited by a
sequence of branch statements, calls, and matching returns.
 \begin{defn}
  \label{defn:path}
  %
  Let program $P \in \lang$ and
  %
  control locations $L = [ \cc{L}_0, \ldots, \cc{L}_{n - 1} ] \in
  \locs^{*}$ be such that the following conditions hold.

  % condition for branch statements
  \textbf{(1)} For each $0 \leq i < n$ such that $\cc{L}_i \in
  \brlocs$, there is a branch statement $b \in P$ such that
  $\prelocof{b} = \cc{L}_i$ and $\brtargetof{b} = \cc{L}_{i + 1}$.

  % condition on call and return statements
  \textbf{(2)} There is a nesting relation $\neststo \in \nestings{n}$
  such that the domain and range of $\neststo$ are exactly the indices
  of the call and successors of return locations in $L$.
  %
  For all $0 \leq i < j < n$ such that $i \neststo j + 1$, there is
  some call statement $c \in P$ such that $\cc{L}_{i + 1} =
  \entryof{c}$ and $\cc{L}_{j + 1} = \rettargetof{c}$, and
  % conditions on return statement:
  some return statement $r \in P$ such that $\cc{L}_{j} =
  \prelocof{r}$.

  %
  Then $[ \cc{L}_0, \ldots, \cc{L}_{n - 1} ]$ is a \emph{path} of $P$.
\end{defn}
% projection:
For each program $P \in \lang$, the space of paths of $P$ is denoted
$\pathsof{P}$, and the set of all paths is denoted $\paths$.
%
For each path $p \in \paths$, we denote the locations and nesting
relation of $p$ as $\locsof{p}$ and $\nestsof{p}$, respectively.

% define space of stores:
Let the space of program values be the space of integers;
%
i.e., the space of values is $\values = \ints$.
%
Our actual implementation of \sys can verify programs that operate on
objects and arrays in addition to integers, but we present \sys as
verifying programs that operate on only integers in order to simplify
the presentation.
% stores:
An evaluation of all variables in $\vars$ is a store;
%
i.e., the space of stores is $\stores = \vars \to \values$.

% Define transition relation of each instruction:
For each instruction $\cc{i} \in \instrs$, there is a transition
relation $\transrelof{\cc{i}} \subseteq \stores \times \stores$.
%
For each branch statement $b \in \brseqs$, the transition relation of
the instruction in $b$ is denoted $\transrelof{b} =
\transrelof{\instrof{b}}$.
%
The transition relation of an instruction need not be total: thus,
branch statements can implement control branches using instructions
that act as \cc{assume} instructions.
%
The transition relation that relates each store at a callsite to the
resulting entry store in a callee is denotes $\calltransrel \subseteq
\stores \times \stores$.
%
The transition relation that relates each calling store, exit store of
a callee, and resulting return store in the caller is denoted
$\rettransrel \subseteq \stores \times \stores \times \stores$.

% Define location at, subrange.
For each space $X$, sequence $s \in X^{*}$, and all $0 \leq i < |X|$,
let the $i$th element in $X$ be denoted $s[i] \in X$.
%
Let the first and last elements of $s$ in particular be denoted
$\headof{s} = s[0]$ and $\lastof{s} = s[|s| - 1]$.

% Runs:
A run of a program $P$ is a sequence of stores $\Sigma$ and a path $p$
of equal length, such that adjacent stores in $\Sigma$ satisfy
transition relations of statements of $P$ at their corresponding
locations in $p$.
%
\begin{defn}
  \label{defn:runs}
  % Introduce states, nesting relation.
  Let $P \in \lang$ be a program,
  %
  let $\Sigma = \sigma_0, \ldots, \sigma_{n - 1} \in \stores$ be a
  sequence of stores, and
  %
  let $q \in \pathsof{P}$ be such that $|\locsof{q}| = n$, such that
  the following conditions hold:

  % case: branch statement:
  \textbf{(1)} For each $i < n - 1$, $(\sigma_i, \sigma_{i + 1}) \in
  \transrelof{\brat{P}{\cc{L}_i}{\cc{L}_{i + 1}}}$.

  % case: call and return statements:
  \textbf{(2)} For each $i < j < n - 1$ such that $i \neststo j + 1$,
  % call step,
  $(\sigma_i, \sigma_{i + 1}) \in \calltransrel$,
  % return step,
  $(\sigma_i, \sigma_j, \sigma_{j + 1}) \in \rettransrel$.

  %
  Then $\Sigma$ is a \emph{run} of $q$ in $P$.
\end{defn}
%
For each path $p \in \paths$, the space of runs of $p$ is denoted
$\runsof{p} \subseteq \stores^{*}$.

% logic, interpolation
\subsection{Formal Logic}
\label{sec:logic}
% define theories:
Our approach uses formal logic to model the semantics of programs and
prove that a given program satisfies a bound.
%
A \emph{theory} is a vocabulary of function symbols and a standard
model.
% define formulas:
For each theory $\mathcal{T}$ and space of logical variables $X$, let
the spaces of $\mathcal{T}$ terms and formulas over $X$ be denoted
$\tterms{X}$ and $\tformulas{X}$, respectively.
%
For each formula $\varphi \in \tformulas{X}$, the set of variable
symbols that occur in $\varphi$ (i.e., the \emph{vocabulary} of
$\varphi$) is denoted $\vocab(\varphi)$.
%
Each term constructed by applying only function symbols in
$\mathcal{T}$ is a \emph{ground term} of $\mathcal{T}$;
%
i.e., the space of ground terms of $\mathcal{T}$ is
$\groundterms{\mathcal{T}} = \tterms{\emptyset}$.

% Define replacement and substitution:
For all vectors of variables $X = [ x_0, \ldots, x_n ]$ and $Y = [
y_0, \ldots, y_n ]$, the formula constraining the equality of each
element in $X$ with its corresponding element in $Y$, i.e., the
formula $\bigland_{0 \leq i \leq n} \cc{x}_i = \cc{y}_i$, is denoted
$X = Y$.
%
For each vector of terms $T_Y = [ t_0, \ldots, t_{n} ]$, the repeated
replacement of variables $\varphi[ \ldots [ t_0 / x_0 ] \ldots t_{n -
  1} / x_{n - 1} ]$ is denoted $\replace{\varphi}{T_Y}{X}$.
%
For each formula $\varphi$ defined over free variables $X$, the
substitution of $Y$ in $\varphi$ is denoted $\subs{\varphi}{Y} \equiv
\replace{\varphi}{X}{Y}$.

% define models:
A \emph{domain} is a finite set of values.
%
For each theory $\mathcal{T}$, a \emph{model} of $\mathcal{T}$ is a
domain $D$ and a map from each $k$-ary function symbol in
$\mathcal{T}$ to a $k$-ary function over $D$.
%
The standard model of a theory $\mathcal{T}$ is a distinguished model
of $\mathcal{T}$.
%
The domain of the standard model of $\mathcal{T}$ is denoted
$\domainof{\mathcal{T}}$.

% extensions
For theories $\mathcal{T}_0$ and $\mathcal{T}_1$, $\mathcal{T}_1$ is
an \emph{extension} of $\mathcal{T}_0$ if the vocabulary of
$\mathcal{T}_0$ is contained by the vocabulary of $\mathcal{T}_1$ and
the standard model of $\mathcal{T}_0$ is the restriction of the
standard model of $\mathcal{T}_1$ to the vocabulary of
$\mathcal{T}_0$.
% combinations:
For all theories $\mathcal{T}_0$ and $\mathcal{T}_1$ whose standard
models are equal on all symbols in the common vocabulary of
$\mathcal{T}_0$ and $\mathcal{T}_1$, the combination~\cite{nelson79}
of theories $\mathcal{T}_0$ and $\mathcal{T}_1$ is denoted
$\mathcal{T}_0 \union \mathcal{T}_1$.
%
We only consider theories $\mathcal{T}$ with standard model $m$ that
maps to domain $D$ such that for each element $d \in D$, there is a
ground term $\termof{d} \in \groundterms{T}$ such that $m(\termof{d})
= d$ (e.g., theories of arithmetic), along with their combinations
with the theory of uninterpreted functions (\euflia).

% Define satisfaction and entailment.
For each theory $\mathcal{T}$, formula $\varphi \in \tformulas{X}$,
and assignment $m$ of $X$ to the domain of $\mathcal{T}$, $m$
\emph{satisfies} $\varphi$ if $\varphi$ evaluates to $\true$ under $m$
combined with the standard model of $\mathcal{T}$ (denoted $m \tsats
\varphi$).
%
For all $\mathcal{T}$ formulas $\varphi_0, \ldots, \varphi_n, \varphi
\in \formulas{\mathcal{T}}$, we denote that $\varphi_0, \ldots,
\varphi_n$ \emph{entail} $\varphi_n$ as $\varphi_0, \ldots, \varphi_n
\tentails \varphi$.
%
A $\mathcal{T}$-formula $\varphi$ is a \emph{theorem} of $\mathcal{T}$
if $\tentails \varphi$.

% Introduce decision procedure:
Although determining the satisfiability of formulas in theories
required to model the semantics of practical languages, such as \lia,
is NP-complete in general, solvers have been proposed that often
efficiently determine the satisfiability of formulas that arise from
practical verification problems~\cite{demoura08}.
%
Our approach assumes access to a decision procedure for \euflia, named
$\eufliaissat$.

\subsubsection{Symbolic Representation of Program Semantics}
\label{sec:symbolic-semantics}
%
The semantics of $\lang$ can be represented symbolically using \lia
formulas.
%
In particular, each program store $\sigma \in \stores$ corresponds to
a \lia model over the vocabulary $\vars$, denoted
$\modelof{\sigma}$.
%
For each space of indices $I$ and index $i \in I$, the space of
variables $\vars_i$ denotes a distinct copy of the variables in
$\vars$, as does $\vars'$, which will typically be used to represent
the post-state of a sequence of transitions.
%
For theory $\mathcal{T}$, the space of program \emph{summaries} is
$\summaries = \tformulas{\vars, \vars'}$.

% symbolic relation for instructions:
For each instruction $\cc{i} \in \instrs$, there is a formula
$\symrelof{\cc{i}} \in \liaformulas{\vars, \vars'}$ such that for all
stores $\sigma, \sigma' \in \stores$, $(\sigma, \sigma') \in
\transrelof{\cc{i}}$ if and only if $\modelof{\sigma},
\modelof{\sigma'} \sats \symrelof{\cc{i}}$.
% symbolic relation for calls:
There is a formula $\callsymrel \in \liaformulas{\vars, \vars'}$ such
that for all stores $\sigma, \sigma \in \stores$, $(\sigma, \sigma')
\in \calltransrel$ if and only if $\modelof{\sigma}, \modelof{\sigma'}
\sats \callsymrel$.
% symbolic relation for returns:
There is a formula $\retsymrel \in \liaformulas{\vars_0, \vars_1,
  \vars_2}$ such that for all stores $\sigma_0, \sigma_1, \sigma_2 \in
\stores$, $(\sigma_0, \sigma_1, \sigma_2) \in \rettransrel$ if and
only if $\modelof{\sigma_0}, \modelof{\sigma_1}, \modelof{\sigma_2}
\sats \retsymrel$.

% Define the problem:
Each program $P$ and logical formula $B$ over the initial state of $P$
and final value of the variable $\cost$ defines a bound-satisfaction
problem.
%
The problem is to decide if over each run $r$ of $P$, the initial
state of $P$ and the value of $\costvar$ in the final state of $r$
satisfy $B$.
%
For theory $\mathcal{T}$, the space of bound constraints is denoted
$\tboundctrs = \tformulas{\add{\vars}{\costvar'}}$.
%
\begin{defn}
  \label{defn:problem}
  % define bound satisfaction for paths, programs
  For each extension $\mathcal{T}$ of \lia, program $P \in \lang$,
  bound constraint $B \in \tboundctrs$, and path $q \in \pathsof{P}$,
  %
  if for each run $r \in \runsof{q}$, it holds that
  $\modelof{\headof{r}}, \costvar' \mapsto
  \modelof{\lastof{r}}(\costvar) \sats B$, then $q$ \emph{satisfies}
  $B$.
  %
  For each path $q \in \pathsof{P}$ it holds that $q$ satisfies $B$,
  then $P$ satisfies $B$, denoted $P \sats B$.
  % define problem:
  The bound-satisfaction problem $(P, B)$ is to determine if $P \sats
  B$.
\end{defn}

% punt on arrays:
While we present our verifier \sys for a simple language whose
semantics can be modeled using only \lia, practical languages
typically provide features that can only be directly modeled using
\lia in combination with the theories of uninterpreted functions and
the theory of arrays.
%
The complete implementation of \sys supports such language features
(see \autoref{sec:evaluation}).

% interpolation:
\subsubsection{Interpolation}
\label{sec:itps}
% Tree-interpolation:
Tree-interpolation problems formulate the problem of finding valid
invariants for all runs of a particular program path that contains
calls and returns~\cite{heizmann10}.
%
The branching structure in the tree-interpolation problem models the
dependency of the result of a function call on the effect of the path
through the callee combined with the arguments provided to the callee
by the caller.
%
\begin{defn}
  \label{defn:tree-itps}
  % Define tree-interpolation problems.
  For theory $\mathcal{T}$, a $\mathcal{T}$-\emph{tree-interpolation
    problem} is a triple $(N, E, C)$ in which:
  %
  \begin{itemize}
  \item
    % Nodes:
    $N$ is a set of \emph{nodes}.
  \item
    % Edges:
    $E \subseteq N \times N$ is a set of \emph{edges} such that the
    graph $T = (N, E)$ is a tree with root $r \in N$.
  \item
    % Constraints:
    $C: N \to \tformulas{X}$ assigns each node to an $\mathcal{T}$
    \emph{constraint}.
  \end{itemize}

  % Define interpolants for tree-interpolation problems.
  For each tree-interpolation problem $T = (N, E, C)$, an
  \emph{interpolant} of $T$ is an assignment $I: N \to \tformulas{X}$
  from each node to a $\mathcal{T}$ formula such that:
  %
  \begin{itemize}
  \item
    % root interpolant entails false
    The interpolant at the root $r \in N$ of $T$ entails $\false$.
    %
    I.e., $I(r) \tentails \false$.
  \item
    % children interpolants entail node interpolant
    For each node $n \in N$, the interpolants at the children of $n$
    and the constraint at $n$ entail the interpolant at $n$.
    %
    I.e., $\{ I(m) \}_{(m, n) \in E}, C(n) \tentails I(n)$.
  \item
    % vocabulary locality
    For each node $n$, the \emph{vocabulary} at $n$ is the common
    vocabulary of all descendants for $n$ and all non-descendants of
    $n$.
    %
    The vocabulary of the interpolant at $n$ is contained in the
    vocabulary of $n$.
    %
    I.e.,
    \begin{align*}
      \vocab(n) = &
      %
      \bigunion_{(m, n) \in E^{*}} \vocab(C(m))
      %
      \intersection \bigunion_{(m', n) \notin E^{*}} \vocab(C(m')) \\
      %
      \vocab(I(n)) \subseteq & \vocab(n)
    \end{align*}
  \end{itemize}
\end{defn}
%
For theory $\mathcal{T}$ and variables $X$, the space of all
tree-interpolation problems whose constraints are $\mathcal{T}$
formulas over $X$ is denoted $\itpproblems{\mathcal{T}}{X}$.
%
For each tree-interpolation problem $T \in
\itpproblems{\mathcal{T}}{X}$, the nodes, edges, root, and constraints
of $P$ are denoted $\nodesof{T}$, $\edgesof{T}$, $\rootof{T}$, and
$\ctrsof{T}$, respectively.
%
The conjunction of all constraints in $T$ is denoted $\ctrof{T} =
\bigland_{n \in \nodesof{T}} C(n)$.
%
A model of $\ctrof{T}$ is referred to alternatively as a model of $T$.

% restrict:
For each tree-interpolation problem $T$ and node $n \in \nodesof{T}$,
the tree-interpolation problem formed by the restriction of $T$ to the
subtree with root $n$ is denoted $\restrict{T}{n}$.
% merge interpolation problems:
The procedure $\mergetrees$ takes two tree-interpolation problems $(N,
E, C)$ and $(N', E', C')$ with $(N', E')$ a subtree of $(N, E)$ and
constructs a tree-interpolation problem in which the constraint for
each node is the conjunction over constraints for all nodes in $N'$.
%
I.e., $\mergetrees((N, E, C), (N', E', C')) = (N, E, C'')$ with
$C''(n) = C(n)$ for each $n \in N \setminus N'$ and $C''(n) = C(n)
\land C'(n)$ for each $n \in N'$.

% weakening problems:
For theory $\mathcal{T}$ and $\mathcal{T}$-interpolation problems
$U_0$ and $U_1$ containing the same nodes and edges, $U_0$ is as weak
as $U_1$ if for each node $n$, the constraint in $U_0$ for $n$ is as
weak as the constraint for $n$ in $U_1$ and the vocabulary of the
constraint in $U_1$ is contained by the vocabulary of the constraint
in $U_0$.
%
\begin{defn}
  \label{defn:weakening}
  For theory $\mathcal{T}$, variables $X$, and $U_0, U_1 \in
  \itpproblems{\mathcal{T}}{X}$, if for $N = \nodesof{T} =
  \nodesof{U}$, $\edgesof{T} = \edgesof{U}$, and for each $n \in N$,
  % constraints are weaker:
  \textbf{(1)} $\ctrof{U_0}(n) \entails \ctrof{U_1}(n)$ and
  % vocab is a restriction:
  \textbf{(2)} $\vocab(\ctrof{U_0}(n)) \subseteq
  \vocab(\ctrof{U_1}(n))$,
  %
  then $U_1$ is \emph{as weak as} $U_0$.
\end{defn}
% weakenings and interpolants
Because weaker interpolation problems have weaker constraints per
node, they admit \emph{fewer} interpolants.
%
\begin{lemma}
  \label{lemma:weak-itps}
  For theory $\mathcal{T}$, variables $X$, all $U_0, U_1 \in
  \itpproblems{T}{X}$ with common nodes $N$ such that $U_1$ as weak as
  $U_0$, and all $I: N \to \tformulas{X}$ such that $I$ is an
  interpolant of $U_1$, $I$ is an interpolant of $U_0$.
\end{lemma}

% theorem provers:
For theory $\mathcal{T}$, an interpolating theorem prover takes a
$\mathcal{T}$-interpolation problem $T$ and returns either a
$\mathcal{T}$-model or a map from the nodes of $T$ to
$\mathcal{T}$-formulas.
%
An interpolating theorem prover is sound if it only returns a valid
model or interpolant of its input.
%
\begin{defn}
  \label{defn:itp-prover}
  For theory $\mathcal{T}$, variables $X$, let effective procedure $t:
  \itpproblems{\mathcal{T}}{X} \to (X \to \domainof{\mathcal{T}})
  \union (\nodesof{T} \to \tformulas{X})$ be such that
  %
  for each tree-interpolation problem $U \in
  \itpproblems{\mathcal{T}}{X}$,
  %
  \textbf{(1)} if $t(U): X \to \domainof{\mathcal{T}}$, then $t(U)$ is
  a model of $U$;
  %
  \textbf{(2)} if $t(U): \nodesof{T} \to \tformulas{X}$, then $t(U)$
  are interpolants of $U$.
  %
  Then $t$ is a \emph{sound} interpolating theorem prover for
  $\mathcal{T}$.
\end{defn}

% Talk about solvers for the DAG interpolation problem.
Previous work has presented an algorithm $\eufliaitps$ that solves a
given $\euflia$ tree-interpolation problem $T$ by invoking an
interpolating theorem prover for \euflia a number of times bounded by
$|\nodesof{T}|$~\cite{heizmann10}.

% fix theories
In \autoref{sec:approach}, we describe an approach for proving that a
program whose semantics are expressed in a theory $\mathcal{T}_0$
satisfies a bound expressed in an extension $\mathcal{T}$.
%
To simplify the presentation of our approach, we fix $\mathcal{T}_0$
to be \lia, and fix $\mathcal{T}$ to be an arbitrary extension of
$\lia$.
%
However, our approach can be applied using any theory for
$\mathcal{T}_0$ that satisfies the above conditions: in particular,
our actual implementation of \sys uses the combination of the theories
of linear arithmetic, uninterpreted functions with equality, and
arrays as its base theory.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
