\section{Related Work}
\label{sec:related-work}
% Complexity analysis:
\paragraph{Automated Complexity Analysis}
%
The problem of bound \emph{analysis}, i.e., taking a program $P$ and
inferring a sound bound on the resources used by $P$, has been the
subject of significant previous work.
% speed:
\speed infers a symbolic bound on the execution time of a program $P$
by (1) instrumenting $P$ to form a new program $P'$ that stores in
counter variables how often it has executed particular control
locations, (2) generates numerical invariants over program and counter
variables, and (3) from the generated invariants, derives bounds on
the execution time of $P$~\cite{gulavani08,gulwani09a,gulwani09b}.
% Reachability-bound problem:
Another approach generates a transition system as a disjunctive binary
relation by selectively expanding the control-flow graph, and then
deriving ranking functions by applying rules from a proof system,
using pattern matching~\cite{gulwani10}.
% Cost analysis of object-oriented bytecode.
Another approach analyzes Java bytecode under a given cost model, and
reduces the problem of inferring a symbolic bound to solving a set of
recursive equations defined from an abstraction interpretation that
models the change in size of data objects as a result of each program
instruction~\cite{albert12}.
%
Another approach derives a complexity bound for a given program $P$
by modeling $P$ as a lossy vector addition system (VASS) and
constructing a lexicographic linear ranking function that proves that
the VASS terminates~\cite{sinn14}.
% Multi-dimensional Rankings, Program Termination, and
% Complexity Bounds of Flowchart Programs
Another approach infers ranking functions for a program via linear
programming and derives a bound from the ranking
functions~\cite{alias10}.

% Type-based analysis.
Several approaches derive bounds by extending type systems to contain
information about the resources used to perform a computation.
%
One approach presents a type system and inference algorithm that
infers polynomial bounds, given the maximal degree of the polynomial
of all bound functions, with the restriction that each term in each
polynomial contains a single variable~\cite{hoffman10}.
%
Further work extends the approach to infer bounds over multi-variate
polynomials~\cite{hoffman11}.
%
Such approaches have been formulated as compositional rules for
deriving bounds of programs, which generate bounds with proofs that
can be independently certified
efficiently~\cite{carbonneaux14,carbonneaux15}.

% us
\sys addresses a problem that is related to, but distinct from, bound
analysis, namely bound \emph{verification}.
%
I.e., \sys takes a taking a program $P$ \emph{and an expected bound
  $B$} and attempts to decide if $P$ satisfies $B$.
%
A key payoff of bound analyses is that their results can potentially
provide useful information to a programmer about the resources used by
their program, while requiring no additional effort from the
programmer.
%
A key payoff of bound verification is that, when successful, it can
provide definite information about a critical bound, including
concrete counterexamples that prove that a program does \emph{not}
satisfy an expected bound.

% better together:
We believe that because bound analyses and \sys were designed to
address distinct problems and are based on complementary techniques
(namely, abstract interpretation and type-checking, compared to model
checking), further work could improve the state of the art of
automated reasoning about program complexity by combining bound
analyses with bound verifiers, such as \sys.
%
In particular, a bound analysis could be used to find sound initial
bounds for a program module, which a bound verifier could further
strengthen by enumerating tighter bounds and either verifying them or
refuting them.
%
Techniques used in \sys for selecting path invariants, in particular
the grounding algorithm at the core of \sys's automatic theorem prover
(\autoref{sec:gen-grounding}) could be used to select relevant
non-linear terms for synthesizing non-linear numerical domains, a
critical problem in developing effective bound
analyses~\cite{gulavani08}.

% Isil's paper in PLDI
\clarity attempts to find a specific class of performance bugs in
programs that maintain collections in data structures~\cite{olivo15}.
%
In particular, \clarity uses points-to analysis to determine if a
program traverses a collection multiple times without modifying the
collection and, if so, determines that the program has a performance
bug.
%
\clarity aggressively applies information about its specific problem
context that enable it to analyze far larger codebases than \sys;
%
\sys can potentially verify or disprove a more general class of
step-count bounds than \clarity.

\paragraph{Worst-Case Execution Time}
% interpretation.
The \emph{worst-case execution time (WCET)} problem is to determine
the worst possible execution time of a program~\cite{wilhelm08}.
%
WCET problems are typically defined for real-time programs and
systems, for which all iterations and recursion are explicitly
bounded.
%
Analyses that address the WCET problem are primarily concerned with
accurately modeling the effects of various complex features of
hardware state that may affect performance, such as the cache and
branch speculation.
%
While our work also addresses a problem related to predicting the
performance of a program, we assume that a model of the cost of each
instruction is given, and primarily consider the problem of reasoning
accurately about unbounded iterations and recursion.

\paragraph{Interpolation-Based Verification}
%
Several model checkers have been proposed that use Craig interpolants
to select an abstraction under which to verify a program.
% BLAST:
\blast uses interpolants to select the predicates that it collects to
define an abstraction in a predicate abstraction of the state space of
a program~\cite{henzinger04}.
% IMPACT
\impact uses conjunctions of the actual interpolants used to refute a
program path as the abstraction of a program~\cite{mcmillan06};
%
such interpolants are found by solving graph-interpolation problems
defined by \emph{linear} dependency graphs.
% Nested interpolants.
An approach that maintains an automaton of nested words annotated with
interpolants~\cite{heizmann10} uses interpolants to refute an
interprocedural trace with facts, that at each point in the trace,
refer only to variables that are local to the procedure;
%
such interpolants are found by solving graph-interpolation problems
defined by \emph{tree} dependency graphs.
%
\sys proves that a given program path satisfies a given resource bound
by reducing the search for inductive step-count invariants to finding
a tree interpolant~\cite{heizmann10} over a theory that is potentially
richer than standard theories that allow interpolation.
%
The key technique employed by \sys is to lazily ground axioms of such
theories, using the structure of associated variables in the original
formula.

% Instantiation based interpolation
Similar to \sys, verifiers of shape properties that implement
instantiation-based interpolation~\cite{stokkermans08,totla13}
instantiate universally-quantified axioms of an extension theory for
describing program heaps until a given path formula becomes
unsatisfiable a base theory that supports interpolation.
%
The key advantage of such approaches is that by using local
extensions, they typically satisfy strong completeness guarantees.
%
While the theorem prover used by \sys is only a semi-decision
procedure for theory extensions, it can be applied to non-local
extensions, allowing it to potentially prove satisfaction of bounds
expressed in non-linear integer arithmetic.

\paragraph{Automated Reasoning}
% Foundational work over reals
Decision procedures for solving non-linear constraints are a topic of
extensive study in the foundations of mathematics, and have been
applied to various problems in program and system verification.
%
Classic and modern results provide decision procedures for solving
quantified formulas with polynomials over real-closed
fields~\cite{collins98,dai13,jovanovic12,tarski51}.
%
These techniques likely cannot be directly applied to discharge
verification conditions of programs or program paths, where the model
of each formula must correspond to a run of a program that computes
integer values.

% Over integers.
While solving non-linear integer formulas is undecidable in general,
recent approaches have been proposed for solving such formulas using
SAT solvers~\cite{borralleras09}.
%
While such a solver could potentially be used to decide if a
particular path of a program in a language with a restricted set of
features satisfies a bound, it is not clear how such a solver can be
used to solve formulas in theories that are a combination of
non-linear arithmetic with other theories used to model practical
language features, such as \euf of the theory of arrays.
%
Furthermore, it is not clear if such approaches can be extended to
provide a proof of bound satisfaction that can be used to prove bound
satisfaction of over related program paths.

% sygus:
Several synthesis
techniques~\cite{udupa13,gulwani11,harris11,reynolds15} have been
unified as instances of syntax-guided synthesis~\cite{alur15}, which
is a meta-technique for synthesizing programs and program invariants
as guided by a syntactic restriction.
%
While \sys can likely be expressed as an instance of \sygus, \sys's
technique for enumerating terms as restricted by vocabularies of
subformulas is distinct from search strategies proposed in all prior
work on \textsc{SyGus}, and is critical to ensuring that path
invariants can be extracted as interpolants from the resulting
unsatisfiable formula.
%
Other \textsc{SyGus} techniques often implement a search for program
terms by solving a suitable system of symbolic
constraints~\cite{gulwani11b,solar05}.
%
\sys's search for grounding terms could potentially be implemented
using symbolic techniques.
%
We leave the development of such techniques as future work.
