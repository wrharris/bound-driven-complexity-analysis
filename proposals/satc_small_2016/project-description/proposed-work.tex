\section{Proposed Work}
\label{sec:proposed-work}
%
In this section, we propose work to be performed over the course of
our project.
%
In \autoref{sec:exhaust-resource}, we review known security
vulnerabilities that cause a program to exhaust resources.
%
In \autoref{sec:ver-data}, we describe existing approaches that verify
properties of a program's data structures.
%
In \autoref{sec:ver-exhaust}, we propose a technical approach that we
will develop that verifies freedom from resource-exhaustion
vulnerabilities by extending known techniques for verifying data
properties.

\subsection{Denial of Service by resource exhaustion}
\label{sec:exhaust-resource}
%
In this section, we review practical attacks on actual systems that
that exhaust program memory (\autoref{sec:exhaust-mem}) and system
resources that the program requests (\autoref{sec:exhaust-sys}).

% talk about memory exhaustion
\subsubsection{Denial of Service by memory exhaustion}
\label{sec:exhaust-mem}

% example resource exhaustion vulnerabilities:
\begin{figure}[t]
  \centering
  \begin{floatrow}
    \ffigbox[0.48\textwidth]{ \input{code/apache-killer.cpp} }
    { \caption{Pseudocode of a vulnerable module of previous versions
        of Apache that make the HTTP server susceptible to a
        memory-exhaustion attack known as the ``Apache Killer.''} %
      \label{fig:apache-killer} }

    % vuln pseudocode for Slowloris vulnerability:
    \ffigbox[0.48\textwidth]{ \input{code/slowloris.cpp} }
    { \caption{Pseudocode of a vulnerable module of a previous
        versions of Apache that make the HTTP server susceptible to a
        connection-exhaustion attack known as ``Slowloris.''} %
      \label{fig:slowloris} }
  \end{floatrow}
\end{figure}
%
Much previous work~\cite{hoffman10,hoffman11}, including the work that
we have performed described in \autoref{sec:previous-work}, presents
techniques for verifying that a given program satisfies an expected
bound on the amount of time that it consumes or that a functional
program uses an amount of space that satisfies an expected bound.
%
However, many DoS attacks on practical programs cause programs
implemented directly in low-level imperative languages to exhaust the
amount of memory available to them to perform a computation.
%
In particular, the first publicly-disclosed DoS vulnerability in
Chrome was triggered by input strings specially crafted from carriage
return symbols, which allowed an attacker to cause Chrome to open a
large number of windows~\cite{cve-2008-4340};
%
similar vulnerabilities have been found in other programs that operate
on data consumed from an untrusted network~\cite{cve-2008-1700}.
%
Previous versions of Microsoft operating systems~\cite{cve-2009-1928},
mail servers~\cite{cve-2006-1173}, and communication software
contained vulnerabilities that allowed an attacker to cause the
program to try to create a stack of unbounded
size~\cite{cve-2009-2726}.
%
Previous versions of Microsoft Office~\cite{cve-2008-5180}, web
application firewalls~\cite{cve-2009-2299}, and the Opera
browser~\cite{cve-2009-2540} allowed an attacker to cause the program
to attempt to allocate an unbounded amount of memory in which to
operate on data.

% apache killer: walk through code:
One critical instance of a vulnerability that enabled an attacker to
cause a program to use an unexpected amount of memory is the ``Apache
Killer'';
%
pseudocode illustrating the vulnerability in Apache is given in
\autoref{fig:apache-killer}.
%
Apache provides a procedure, depicted in \autoref{fig:apache-killer}
as \cc{get_byteranges}, that for a fixed buffer \cc{buffer}, takes a
set of ranges---each represented as a pair of a starting and ending
index in \cc{buf}---and sends the ranges of \cc{buffer} between each
start and endpoint to the client.
%
\cc{get_byteranges} first allocates a queue of buffers (line 4).
%
For each requested range (lines 6--8), \cc{get_byteranges} allocates a
buffer \cc{buf} with size sufficient to store all requested bytes
(lines 10---12), copies the requested range of bytes from \cc{buffer}
into \cc{buf} (line 14), and adds \cc{buf} to \cc{bufs} (line 16).
%
Then, for each allocated buffer \cc{buf} (lines 18--20),
\cc{get_byteranges} sends the contents of \cc{buf} to the client (line
22) and frees \cc{buf} (line 23).

% give expectations
\cc{get_byteranges} was designed under the implicit assumption that a
benevolent client would request a selective set of byteranges that
minimized computation performed on its behalf, and in particular that
do not overlap.
%
In such a context, \cc{bufs} would use an amount of memory that is at
most slightly larger than \cc{buffer}.
%
However, an attacker who attempts to maximize the resources that
\cc{get_byteranges} uses on their behalf can cause \cc{get_byteranges}
to use an amount of memory that is the maximum number of ranges that
can be requested times the size of \cc{buffer}.
%
In practice, causing \cc{get_byteranges} to use such an amount of
memory causes it to exhaust all memory available to it on its host
system, and crash.

% forecast what we're going to deliver
A developer who implements such a server would benefit from a verifier
that enables them to specify that \cc{get_byteranges} should only use
an amount of memory of size under an expected bound.
%
When such a verifier is given \cc{get_byteranges} as depicted in
\autoref{fig:apache-killer} and the size of \cc{buffer} as a bound, it
should generate inputs that cause \cc{get_byteranges} to use an amount
of memory that exceeds the size of \cc{buffer}.
%
I.e., it should generate a sequence of requests for overlapping ranges
of \cc{buffer}.
%
When such a verifier is given a hardened version of
\cc{get_byteranges} that checks that its given ranges are
non-overlapping, the verifier should return program invariants on the
amount of memory used by \cc{get_byteranges} that prove that it always
uses an amount of memory bounded by the size of \cc{buffer}.


% talk about system exhaustion
\subsubsection{Denial of Service by system-resource exhaustion}
\label{sec:exhaust-sys}
%
An attacker can cause a program to exhaust not only available memory
but other critical resources provided by the program's host system.
%
In particular, vulnerabilities in the ClamAV virus scanner and a
communications program developed by Cisco systems and enable an
attacker to exhaust the set of file descriptors that a process running
the program attempts to request from its host operating
system~\cite{cve-2007-0897,cve-2009-2054}.
%
A vulnerability in a previous version of Cisco Teleconferencing
software enables an attacker to create a large number of TCP
connections, to the point that the executing process eventually
crashes~\cite{cve-2009-2874}.

% slowloris: walk through HTTP server code
Slowloris is a similar vulnerability in previous versions of many HTTP
servers, including the Apache Tomcat server~\cite{slowloris}.
%
Pseudocode illustrating the vulnerability is given in
\autoref{fig:slowloris}.
%
The pseudocode depicts a loop in the server that processes incoming
requests.
%
In each iteration of the loop, the server gets the next incoming
packet and stores it in variable \cc{pkt} (line 4).
%
If the packet requests that the server create a new connection (line
7), then the server does so and adds the connection to a pool that it
maintains.
%
Otherwise, the server takes a different appropriate action to service
the request (lines 11--17).
%
\BH{confirm structure, operations}
%
The server keeps each connection open until it completes a session
over each connection satisfactorily, and then closes the connection.
%
The server may also close a connection if it does not receive
communication from the connection's client over a suitable amount of
time.

% give expectations, reality
However, an attacker can cause such an implementation to keep a
prohibitively large number of connections open by flooding a server
with many requests to create connections and, over each connection,
persistently sending packets that cause the server to keep the
connection open.
%
A developer who implements such a server would benefit from a verifier
that enables them to specify that their server should only maintain a
bounded number of open connections.
%
When such a verifier is given the program in \autoref{fig:slowloris},
it would provide an execution on which the server maintains a set of
open connections whose size exceeds the desired bound.
%
When the verifier is given a version of the program in
\autoref{fig:slowloris} that implements a scheduler that evicts
long-running connections more aggressively, the verifier should
confirm that the program maintains only a bounded number of open
connections.

% talk about previous work on shape analysis:
\subsection{Verifying properties of data structures}
\label{sec:ver-data}
%
Conventional logics used in program verification, such as combinations
of the theories of linear arithmetic, uninterpreted functions, and the
theory of arrays, can be directly applied to model the semantics of
programs that primarily perform arithmetic operations on data with
flat structure.
%
While the theory of arrays can be used to accurately model the
semantics of arbitrary loads and stores to a program heap, the theory
cannot easily be used to express, much less automatically infer,
inductive invariants over heap structure needed to support useful
program specifications.
%
In this section, we review logics introduced to overcome limitations
in conventional logics, in particular separation logic
(\autoref{sec:sep-logic}) and reachability logics
(\autoref{sec:reach-logic}).
%
We will refer to such logics collectively as \emph{shape logics}.

\subsubsection{Separation logic}
\label{sec:sep-logic}

% introduction to separation logic:
One significant practical limitation of conventional logics is that
they cannot naturally express the property that no cell in one segment
of a heap contains a pointer to a distinct segment of the heap (i.e.,
that the heap segments are separate).
%
However, separation logic~\cite{reynolds02} contains logical operators
that enable such properties to be expressed succinctly.
%
E.g., separation logic contains the logical operations
from classical logic (e.g., for properties $P$ and $Q$ expressible in
the logic, one can express that a heap $h$ satisfies both properties
$P$ and $Q$ by establishing that $h \sats P \land Q$).
%
However, it also contains a \emph{separating conjunction} operator,
where if a heap $h$ contains disjoint subheaps that satisfy properties
$P$ and $Q$, then $h$ satisfies the separated-conjunction of $P$ and
$Q$, denoted $P * Q$.

% basic usage:
Programming languages with semantics axiomatized in separation logic
have been used to manually design and verify the correctness of
programs that perform complex operations on program heaps, such as a
garbage collector~\cite{birkedal04}.
%
Automatic analyses have been proposed that represent and infer program
invariants as formulas in separation
logic~\cite{calcagano11,distefano06}.

% example resource exhaustion vulnerabilities:
\begin{figure}[t]
  \centering
  \begin{floatrow}
    \ffigbox[0.48\textwidth]{ \input{code/sep-logic.c} }
    { \caption{\cc{part_free}: deallocates part of a given heap.
        %
        Pre and post-conditions that can be expressed and proven valid
        in separation logic are given in \autoref{sec:sep-logic}.} %
      \label{fig:sep-logic} }

    % include example from JACM paper
    \ffigbox[0.48\textwidth]{ \input{code/epr.c} }
    { \caption{\cc{insert}: a procedure that inserts a heap cell into
        a list.
        %
        Pre and post-conditions that can be expressed and proven valid
        in a reachability logic are given in
        \autoref{sec:reach-logic}. }
      \label{fig:insert} }
  \end{floatrow}
\end{figure}

% walk through the running example:
\autoref{fig:sep-logic} contains an example program introduced in
previous work~\cite{calcagano11} that illustrates how separation logic
can be used to express and infer useful summaries of the effect of
program operations on program heaps.
%
\autoref{fig:sep-logic} contains a definition of a type \cc{node} of
cells similar to what would be used to represent a linked list (line
1).
%
The program contains a declaration of a procedure \cc{f} that takes
two pointers to \cc{node}s (line 3) and a procedure \cc{part_free}
that takes a \cc{node} pointer \cc{y} and returns a node pointer (line
3).
%
\cc{part_free} allocates two \cc{node}'s, stores them in variables
\cc{x} and \cc{z}, and sets their \cc{tail} pointers to \cc{NULL}
(lines 4--8).
%
\cc{part_free} then runs \cc{f} on \cc{x} and \cc{y} (line 9), runs
\cc{f} on \cc{x} and \cc{z} (line 10), and returns the \cc{node}
pointer in \cc{x} (line 11).

% walk through the summaries:
Suppose that a developer wishes to express the following summaries for
the procedures given in \autoref{fig:sep-logic}.
%
(1) If \cc{f} is run from a heap in which \cc{x} and \cc{y} point to
disjoint lists, then it results in a heap in which only \cc{x} points
to a list.
%
(2) If \cc{part_free} is run in a heap consisting of separate subheaps
$H$ and a list pointed to by \cc{y}, then it results in a heap
consisting of $H$ and a list pointed-to by \cc{x}.
%
Then the developer can express the two summaries in separation logic
as the following pre and post-conditions:
%
\begin{center}
  % 
  \begin{tabular}{| l | c | c ||}
    \hline
    \textbf{Procedure} & 
    \textbf{Pre-condition} & 
    \textbf{Post-condition} \\
    \hline
    \hline
    % 
    \cc{f} &
    $\mathsf{list}(\cc{x}) * \mathsf{list}(\cc{y}) * H_0$ & 
    $\mathsf{list}(\cc{x}) * H_0$ \\
    \hline
    % 
    \cc{part_free} & 
    $\mathsf{list}(\cc{y}) * H_1$ &
    $\mathsf{list}(\cc{x}) * H_1$ \\
    \hline
  \end{tabular}  
\end{center}
%
An automatic analysis based on separation
logic~\cite{calcagano11,distefano06}, given the summary of \cc{f}, can
automatically prove that \cc{part_free} satisfies the summary given
above.

\subsubsection{Reachability logics}
\label{sec:reach-logic}
% context: limitations of separation logic:
Separation logic was initially introduced to manually verify rich
properties of arbitrary program heaps~\cite{reynolds02}.
%
Further work has demonstrated that it can often be used as a domain to
represent useful program invariants~\cite{calcagano11,distefano06}.
%
However, because it is such an expressive logic, automatic verifiers
can typically provide only weak guarantees about their effectiveness
in inferring invariants.

% overview of reachability logic:
In comparison, \emph{reachability
  logics}~\cite{itzhaky13,itzhaky-ba14,itzhaky-bj14} can only express
properties about which cells in a \emph{linked list} can reach each
other.
%
Useful correctness properties of practical program modules can often
be expressed in terms of reachability.
%
Furthermore, verifiers that express pre-conditions, post-conditions,
and summaries using reachability logics often satisfy strong
properties regarding their relative completeness~\cite{itzhaky-bj14}.

% walk through running example:
\autoref{fig:insert} contains a procedure \cc{insert} introduced in
previous work on reachability logics~\cite{itzhaky-bj14} that, given
pointers \cc{e}, \cc{h}, and \cc{x}, inserts the list cell at \cc{e}
in the list at \cc{h} immediately before the list cell at \cc{x}
(lines 2---10).
%
A developer may wish to prove that if \cc{insert} is run in a heap in
which (1) \cc{h} points to a non-null list, %
(2) \cc{h} reaches \cc{x}, %
(3) \cc{x} reaches \cc{NULL}, %
(4) \cc{e} is not \cc{NULL}, and %
(5) \cc{h} does not reach \cc{e}, then it results in a heap in which
%
(1) \cc{h} is not null, %
(2) \cc{h} reaches \cc{e}, %
(3) \cc{e} points to \cc{x}, and %
(4) \cc{x} reaches \cc{NULL}.
%
The pre-condition and post-condition can be expressed in a
reachability logic as:
%
\begin{align*}
  % pre-condition:
  \text{\textbf{Pre}: } & %
  \cc{h} \not= \cc{NULL} \land %
  \cc{h} \langle \cc{n}^{+} \rangle \cc{NULL} \land %
  \cc{e} \not= \cc{NULL} \land %
  \lnot \cc{h} \langle \cc{n}^{*} \rangle \cc{e} \\
  % % post-condition:
  \text{\textbf{Post}: } & %
  \cc{h} \not= \cc{NULL} \land \cc{h} \land %
  \cc{h} \langle \cc{n}^{*} \rangle \cc{e} \land %
  \cc{e} \langle \cc{n} \rangle \cc{x} \land %
  \cc{x} \langle \cc{n}^{*} \rangle \cc{NULL}
\end{align*}
% explain notation:
Where the predicate $x \langle \cc{n}^{*} \rangle y$ denotes that cell
$x$ reaches cell $y$ over zero or more links of field \cc{n} and the
predicate $x \langle \cc{n}^{+} \rangle y$ denotes that cell $x$
reaches cell $y$ over one or more links of field \cc{n}.

% make claim about finding invariant:
Finding an invariant for the loop in lines 5---7 that proves that
\cc{insert} satisfies the given summary is non-trivial, because such
an invariant must establish that in each iteration of the loop, \cc{p}
points to a cell that is reachable from \cc{h} and either (1) stores
\cc{null} and has never reached a the cell stored in \cc{x} or (2)
stores the cell in \cc{x}.
%
While such an invariant is extremely difficult for an analysis to
infer and express in a conventional shape or separation logic, logics
that use reachability logics can infer such an invariant
efficiently~\cite{itzhaky-bj14}.

% propose new work
\subsection{Verifying freedom from system-resource exhaustion}
\label{sec:ver-exhaust}
%
We propose to develop an automatic program verifier that determines if
a program satisfies an expected bound on the amount of memory or
system resources in a particular state that it consumes (i.e., that
verifies that program satisfy definitions of security discussed in
\autoref{sec:exhaust-resource}) by extending shape logics for
verifying properties of program heaps discussed in
\autoref{sec:ver-data}.
%
To develop such verifiers, we will apply a three-step process to each
of the shape logics described in \autoref{sec:ver-data}.
%
In the first step, we will extend each shape logic with predicates
that can express critical properties of the size of segments of a
program's heap.
%
We will then axiomatize a conventional low-level imperative language,
such as the LLVM intermediate language or Java bytecode, in our logic.

% separation logic for memory cell counting:
In particular, we may express separation logic with constructs that
allow one not only to express that a heap consists of multiple
separate segments, but to express predicates that relate the sizes of
the segments.
%
E.g., we may introduce a separating conjunction that $*[R(l_0, l_1)]$,
where $R$ is a predicate over integer variables $l_0$ and $l_1$, such
that heap $h$ satisfies formula $H_0 *[R(s_0, s_1)] H_1$ if $h$
consists of separate segments $h_0$ that satisfies $H_0$ and $h_1$
that satisfies $h_1$, with the number of cells $s_0$ in $h_0$ and
number of cells $s_1$ in $h_1$ satisfying $R(s_0, s_1)$.
%
Such a logic could be used to express the memory-exhaustion freedom of
\cc{get_byteranges} as $\cc{buffer} *[ s_0 \leq s_1 ] \cc{bufs}$.

% challenges in developing a logic for system-resource usage:
Developing a logic that can express desired properties of system
resources used presents challenges in addition to designing a logic
for expressing properties of memory usage.
%
Such challenges are caused by the fact that system resources are
represented to the program as data extended with state that denotes
what operations the program and system have performed on them.
%
E.g., a file descriptor or network connection may be in one of
multiple states depending on how it has been accessed.

% outline plan for extending logics
To express such properties, we will extend existing shape logics with
constructs that not only count memory cells in a subheap, but that
count the number of resources in particular states.
%
E.g., we may extend reachability logics so that formulas can be
constructed from uninterpreted state predicates that are axiomatized
to model the semantics of a given system API.
%
For memory cells $x$ and $y$, heap field \cc{f}, and state predicate
$q$, the predicate $x \langle (\cc{f}, q)^{N} \rangle y$ would denote
that $x$ reaches $y$ over at most $N$ links on field \cc{f}, and
contains only cells storing system resources in state $q$.
%
A resource usage policy for an HTTP server that prohibits the
Slowloris vulnerability (discussed in \autoref{sec:reach-logic}) by
requiring that a server may only store in connection pool
\cc{conn_pool} $N$ open connections would be expressed in such a logic
as $\cc{conn\_pool} \langle (\cc{next}, \mathsf{open})^{*} \rangle
\cc{NULL}$.

% validation
In the second step of each sub-project, we will develop an automatic
\emph{validator} that uses the logic defined in the first phase to
prove that a program fully annotated with expected pre-conditions,
post-conditions, and loop invariants in the logic satisfies an
expected bound on resource usage.
%
As discussed in \autoref{sec:campy-validation} and
\autoref{sec:ver-data}, developing an automatic validator will amount
to designing and implementing a decision procedure for the new logics
that we will develop.
%
To develop each decision procedure, we will combine previous work on
developing decision procedures for shape
logics~\cite{brotherston14,itzhaky13,itzhaky-ba14,itzhaky-bj14,seshia03}
with our previous work on developing a semi-decision procedure for
logics used to count execution steps taken by a
program~\cite{srikanth17}.

% automatic verifier
In the third step of each sub-project, we will develop an automatic
program \emph{verifier} that uses the logic developed in step (1) and
decision procedure developed in step (2) to express a desired policy
on resources used and verify or falsify it, without requiring the
programmer to provide loop invariants or summaries for internal
procedures that the program uses.
%
As discussed in \autoref{sec:campy-verification} and
\autoref{sec:ver-data}, developing an automatic verifier will amount
to developing an interpolating theorem prover for each logic that we
propose.
%
To develop each theorem prover, we will combine previous work on
developing interpolating theorem provers for shape
logics~\cite{albarghouthi15,drews16} with our previous work on
developing an interpolating theorem prover for logics used to count
execution steps taken by a program~\cite{srikanth17}.

\subsection{Evaluation}
\label{sec:prop-eval}
%
We will evaluate the verifiers that we develop on program modules
collected both from critical systems and programming education.
%
The extensive history of memory and system-resource exhaustion
attacks~\cite{cve-2006-1173,cve-2008-1700,cve-2008-4340,cve-2008-5180,cve-2009-1928,cve-2009-2299,cve-2009-2540,cve-2009-2726}
strongly indicates that many modules of critical systems have subtle
requirements for memory and system-resource usage.
%
In particular, we will collect modules that implement critical system
services, such as Android library services that historically have
contained DoS vulnerabilities~\cite{huang15} and modules of critical
systems that interface with untrusted users, such as servers.

% education
We will also evaluate the verifiers that we develop, particularly for
verifying freedom from memory exhaustion, as automatic aids for
teaching programmers to implement space-efficient algorithms.
%
In particular, we will apply our verifier to the repository of public
programming solutions collected in previous work~\cite{srikanth17}.
%
We will use the benchmarks to evaluate to what extent our verifier can
provide developers with both (1) bugs that explain when their
implementations use a prohibitively large amount of memory and (2)
proofs that explain why their programs will always use an acceptably
low amount of memory.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
