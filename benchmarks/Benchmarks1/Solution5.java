
//14. O(n) once again but also simple and can be implemented by slightly changing it to a bit more complex to fit our contraints
public class Solution5 {    

    public void moveZeroes(int[] nums) {
        int z = -1;
        for (int i=0; i< nums.length; i++) {
            int temp = nums[i];
            if (temp != 0) {
                nums[i]=nums[++z];
                nums[z]=temp;
            }
        }
    }

}
