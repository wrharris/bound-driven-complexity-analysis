% Define the complexity-bound problem.
\section{Problem statement}
%
\label{sec:problem}
%
In this paper, we present a bound-satisfaction analysis for a language
of imperative programs.
%
We first define the structure (\autoref{sec:structure}) and semantics
(\autoref{sec:semantics}) of programs in our target language, and then
define the bound-satisfaction problem (\autoref{sec:bound-sat}).

% Define program structure as CFG.
\subsection{Program structure}
%
\label{sec:structure}
%
A program's control flow may be unstructured;
%
i.e., a control-flow graph may model a program with branches to
arbitrary control locations.
%
Let $\wordsz \in \nats$ be a fixed machine \emph{word size}, and let
$\words = \bools^{\wordsz}$ denote the space of all \emph{machine
  words}.
%
For simplicity, we define all programs using a fixed set of \emph{data
  variables} $\datavars$ that store machine-word values.
%
We denote the set of primed copies of all variables in $\datavars$ as
$\datavars'$.
%
\begin{defn}
  \label{defn:syntax}
  A \emph{program} is a five-tuple $(\locs, \init, \final, \progedges,
  \transrel)$ where
  %
  \begin{itemize}
  \item 
    % Control locations:
    $\locs$ is the set of \emph{control locations}.
  \item 
    % Initial location:
    $\init \in \locs$ is the \emph{initial} control location.
  \item 
    % Final location:
    $\final \in \locs$ is the \emph{final} control location.
  \item
    % Control edges:
    $\progedges \subseteq \locs \times \locs$ is the set of
    \emph{control edges}.
  \item
    % Transition relation:
    $\transrel: \progedges \to \bvformulas[\datavars,\datavars']$ is
    the program \emph{transition relation}.
  \end{itemize}
\end{defn}

% Define semantics as runs of a program.
\subsection{Program semantics}
%
\label{sec:semantics}
%
A program run is a sequence of states from the program's initial
control location to the program's final control location in which each
adjacent pair of states satisfies the program's transition relation.
%
\begin{defn}
  \label{defn:semantics}
  %
  A \emph{valuation} is a map from each data variable to a machine
  word;
  %
  i.e., $\vals = \datavars \to \words$.
  %
  For program $P = (\locs, \init, \final, \progedges, \transrel)$, a
  \emph{state} is a pair of a control location $\locvar \in \locs$ and
  a valuation;
  %
  i.e., $\states = \locs \times \vals$.
  %
  A \emph{run} of program $P$ is a sequence of states $r = \sigma_0,
  \sigma_1, \ldots, \sigma_n \in \states^{*}$ such that:
  %
  \begin{itemize}
  \item
    % First state at first location:
    The first state is at the initial control location of $P$.
    %
    I.e., for some valuation $V \in \vals$, $\sigma_0 = (\init, V)$.
  \item
    % Final state at final control location:
    The final state is at the final control location of $P$.
    %
    I.e., for some valuation $V \in \vals$, $\sigma_n = (\final, V)$.
  \item
    % Successive pairs in the transition relation.
    Each adjacent pair of states in $r$ is in the program's control
    and transition relations.
    %
    I.e., for all states $\sigma_i = (L_i, V_i)$ and $\sigma_{i + 1} =
    (L_{i + 1}, V_{i + 1})$, $(\locvar_i, \locvar_{i + 1}) \in
    \progedges$ and $V_i, V_{i + 1} \sats \transrel(\locvar_i,
    \locvar_{i + 1})$.
  \end{itemize}
\end{defn}

% Define the problem.
\subsection{The bound-satisfaction problem}
%
\label{sec:bound-sat}
% Define the bound-satisfaction problem.
For a given program $P$, cost control location $\costloc$, bound
function $B$ over data variables, and length restriction $l$, the
bounds-checking problem is to determine if each run of $P$ of length
less than or equal to $l$ contains a number of states at $\costloc$
that is bounded by $B$ evaluated on the input values of the run.
%
\begin{defn}
  \label{defn:prog-bound-sat}
  % Define the space of bounds
  A \emph{bound} is a non-linear function over the space of data
  variables $\datavars$;
  %
  we denote the space of bounds as $\bounds$.

  % Define the bound-satisfaction problem.
  For program $P$, cost location $\costloc \in \locs$, bound $B \in
  \bounds$, and length restriction $l \in \nats$, a solution to the
  \emph{bound-satisfaction problem} $(P, \costloc, B, l)$ is a
  decision as to whether, for each run $r = (L_0, V_0), \ldots, (L_{n
    - 1}, V_{n - 1}) \in \states^{*}$ of $P$ with $n \leq l$, the
  number of occurrences of $\costloc$ is less than or equal to
  $B(V_0)$.
\end{defn}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "paper"
%%% End: 
