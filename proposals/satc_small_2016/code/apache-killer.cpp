typedef range = std::pair< int, int >;
void get_byteranges(std::set< range > reqs) {
  // create an empty list of buffers
  std::queue<char*> bufs;
  // for each range in a work list:
  for (std::set<std::pair<range>::iterator
         it = reqs.begin();
       it != reqs.end(); it++) {
    // allocate a buffer that stores the range
    char* buf = (char*)
      malloc((sizeof char) *
             (it->first - it->second));
    // copy the range into the buffer
    range_copy(it->start, it->end, buf);
    // add the allocated buffer to a list
    bufs.push_back(buf); }
  // for each buffer in the list:
  for (std::queue<char*>::iterator
         it = bufs.begin();
       it != bufs.end(); it++) {
    // send the range to the user and free it
    send(*it);
    free(*it); }
  return; }
