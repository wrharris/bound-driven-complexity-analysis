/* 16.O(n) two if contraints and a simple while loop
 * 
 * KEY: non-trivial bound on loop iterations.
 */
public int removeElement(int[] nums, int val) {
    if(nums.length == 0) return 0;
    if(nums.length == 1) return nums[0] == val ? 0 : 1;
    int i=0, j=nums.length-1;
    while(i <= j) {
        if(nums[j] == val) j--;
        else {
            if(nums[i] == val) {
                nums[i] = nums[j];
                j--;
                i++;
            } else {
                i++;
            }
        }
    }
    return i;
}