% Walk through everything in gory detail.
\section{Technical Approach}
\label{sec:approach}
%
In this section, we describe a verifier \sys for the
bound-satisfaction problem.
%
In \autoref{sec:summaries}, we define the summaries that \sys attempts
to infer to prove that a given program satisfies a given bound.
%
In \autoref{sec:verifier}, we give the design of \sys's core
verification algorithm.
%
In \autoref{sec:thm-prover}, we give the design of a
tree-interpolating theorem prover, which is used by \sys to prove that
individual paths satisfy a given bound.
%
In \autoref{sec:correctness}, we discuss key properties of \sys.

% Define invariant hypertrees:
\subsection{Summaries}
\label{sec:summaries}
%
\sys, given a program $P$ and bound $B$, attempts to infer summaries
of the behavior of $P$ that imply that all paths of $P$ satisfy $B$.
%
A program summary is a map from each control location \cc{L} to a
symbolic summary of the effects of all runs from the entry point of
\cc{L}'s procedure to \cc{L}.
%
The space of program summaries is denoted $\progsummaries = \locs \to
\summaries$.
%
Program summaries are inductive for $P$ and $B$ if they imply that all
runs of $P$ satisfy $B$.
%
\begin{defn}
  \label{defn:inductive-summaries}
  For program $P \in \lang$ and bound constraint $B \in \tboundctrs$,
  let $S \in \progsummaries$, be such that:

  % entry locations are entailed by equality:
  \textbf{(1)} for each procedure $f \in \procnms$,
  \[ \vars = \vars' \entails S(\entryof{f}) \]

  % final location entails bound constraint:
  \textbf{(2)} $S(\finalloc) \entails \tboundctrs$;

  % models each branch statement:
  \textbf{(3)} For each branch statement $b \in P$,
  %
  \begin{align*}
    \subs{S(\prelocof{b})}{\vars_0, \vars_1},
    %
    \subs{\symrelof{b}}{ \vars_1, \vars_2 } & \entails \\
    %
    \subs{S(\brtargetof{b})}{\vars_0, \vars_2}
  \end{align*}

  % models each call and return:
  \textbf{(4)} For each call statement $c \in P$ and each return
  statement $r \in P$ that returns to $c$,
  %
  \begin{align*}
    % summary up to call site
    \subs{ S(\prelocof{c}) }{ \vars_0, \vars_1 },
    % and call transition relation,
    \subs{ \callsymrel }{ \vars_1, \vars_2 }, & \\
    % and summary of callee
    \subs{ S(r) }{\vars_2, \vars_3},
    % and return transition relation,
    \subs{ \retsymrel }{ \vars_1, \vars_3, \vars_4 } & \entails \\
    \subs{ S(\rettargetof{c}) }{ \vars_0, \vars_4 } &
  \end{align*}
  %
  Then $S$ are \emph{inductive step-count} summaries for $P$ and
  $B$.
\end{defn}
%
The space of inductive summaries for program $P \in \lang$ and bound
constraint $B \in \tboundctrs$ is denoted $\indsummaries{P}{B}$.
%
\begin{ex}
  \label{ex:ind-summaries}
  The step-count invariants given in \autoref{sec:inductive-invs}
  directly define inductive step-count summaries for
  \cc{BinarySearch} and $\binsearchbound$.
\end{ex}

% summaries as evidence:
Inductive summaries are evidence of bound satisfaction.
%
\begin{lemma}
  \label{lemma:bound-sat-evidence}
  For each program $P \in \lang$ and bound $B \in \tboundctrs$, if
  there are inductive summaries $S \in \indsummaries{P}{B}$, then $P
  \sats B$.
\end{lemma}

% define visible suffixes:
For path $p$, a visible suffix of $p$ is a sequence of locations in
$p$ connected over only branch edges, nesting edges, and return edges.
%
\begin{defn}
  \label{defn:vis-suffix}
  For each path $p \in \paths$, let $L \in \locs^{*}$ be such that
  %
  $\lastof{L} = \finalloc$
  %
  and there is some function $m: \ints_{|L|} \to \ints_{|p|}$ such
  that
  %
  for each $0 \leq i < |L|$, $L[i] = p[m(i)]$,
  %
  if $L[i] \in \brlocs$ or $L[i] \in \retlocs$, then $L[i + 1] =
  p[m(i) + 1]$, and %
  if $L[i] \in \calllocs$, then for $j < |p|$ such that $i \nestsof{p}
  j$, $L[i + 1] = p[j]$.
  %
  Then $L$ is a \emph{visible suffix} of $p$.
\end{defn}
%
For each path $p \in \paths$, let the space of visible suffixes of $p$
be denoted $\vissuffixesof{p} \subseteq \locs^{*}$.
%
For program $P \in \lang$, the visible suffixes of all paths of $P$
are denoted $\vissuffixesof{P} = \bigunion_{p \in \pathsof{P}}
\vissuffixesof{p}$.

% define path summaries:
Path summaries are sets of visible suffixes of a program's paths, with
each visible suffix $s$ mapped to a summary of the effect of all runs
of $s$.
%
\begin{defn}
  \label{defn:vis-summaries}
  For program $P \in \lang$, let $Q \subseteq \vissuffixesof{P}$ be
  visible suffixes of paths of $P$, and let $S: Q \to \summaries$.
  %
  Then $(Q, S)$ are \emph{visible suffix summaries} of $P$.
\end{defn}
%
For program $P \in \lang$, the space of all visible suffix summaries
of $P$ is denoted $\pathsummariesof{P}$.
%
For all visible suffix summaries $S \in \pathsummariesof{P}$, the
visible suffixes and summary map of $S$ are denoted
$\vissuffixesof{S}$ and $\sumsof{S}$.

% define valid path summaries:
If visible suffix summaries soundly model the semantics of the paths
of which the summaries are subsequences, then the summaries are valid.
%
\begin{defn}
  \label{defn:valid-summaries}
  For program $P \in \lang$, let visible-suffix summaries $S \in
  \pathsummariesof{P}$ be such that for each visible suffix $s \in
  \suffixesof{S}$,
  % models procedure entry:
  \textbf{(1)} for each procedure $f \in \procnms$, if $\headof{s} =
  \entryof{f}$, then
  \[ \vars = \vars' \entails \sumsof{S}(s) \]
  % models branch statement:
  \textbf{(2)} for each branch statement $b \in P$ such that
  $\brtargetof{b} = \headof{s}$ and $s_0 = \prelocof{b} \cons s \in
  P$,
  %
  \begin{align*}
    \subs{\sumsof{S}(s_0)}{\vars_0, \vars_1}, %
    \subs{\symrelof{b}}{\vars_1, \vars_2} & \entails \\
    \replace{\sumsof{S}(s)}{\vars_0}{\vars_2} &
  \end{align*}
  % models call and return:
  \textbf{(3)} and each call statement $c \in P$ such that $s_0 =
  \prelocof{c} \cons s \in \suffixesof{S}$ and return statement $r \in
  P$ such that $\procof{c} = \procof{r}$ and $s_1 = r \cons s \in
  \suffixesof{S}$,
  \begin{align*}
    \subs{\sumsof{S}(s_0)}{\vars_0, \vars_1},
    \subs{\callsymrel}{\vars_1, \vars_2} & \\
    \subs{\sumsof{S}(s_1)}{\vars_2, \vars_3},
    \subs{\retsymrel}{\vars_1, \vars_3, \vars_4} & \entails \\
    \subs{\sumsof{S}(s)}{\vars_0, \vars_4} &
  \end{align*}
\end{defn}
% callback to running example in overview:
\begin{ex}
  \label{ex:path-sums}
  In \autoref{sec:inferring-invs}, \autoref{fig:bin-search-path}
  contains path invariants for path $q$ that define valid
  visible-suffix summaries of \cc{BinarySearch} over all suffixes of
  $q$.
\end{ex}

% Define inductive summaries from path summaries:
Path summaries define inductive summaries of $P$ when they define
summaries of all paths of $P$.
%
\begin{defn}
  \label{defn:inductive-path-summaries}
  For each program $P \in \lang$ and bound constraint $B \in
  \tboundctrs$, let $S \in \pathsummariesof{P}$ be valid suffix
  summaries.
  %
  Let $S' \in \progsummaries$ be such that for each location $\cc{L}
  \in \locs$,
  %
  \begin{align*}
    S'(\cc{L}) = %
    \biglor \setformer{ \sumsof{S}(t) }{ %
      t \in \suffixesof{S}, \headof{t} = \cc{L}}
  \end{align*}
  %
  If $S'$ are inductive summaries for $P$ and $B$
  (\autoref{defn:inductive-summaries}), then $S$ are \emph{inductive
    visible-suffix summaries} for $P$ and $B$.
\end{defn}
% Connect to verification algorithm:
\sys attempts to prove that a given program $P$ satisfies a given
resource bound $B$ by inferring inductive visible-suffix summaries for
$P$ and $B$.

% Give the main verification algorithm:
\subsection{Verification Algorithm}
\label{sec:verifier}

\begin{algorithm}[t]
  % Declare IO markers.
  \SetKwInOut{Input}{Input}
  %
  \SetKwInOut{Output}{Output}
  % Declare sub-program (procedure) markers.
  \SetKwProg{myproc}{Procedure}{}{}
  % Inputs: a program
  \Input{A program $P \in \lang$ and resource bound $B \in
    \tboundctrs$.}
  % Output: decision for safety
  \Output{A decision as to whether $P$ satisfies $B$.}
  % verify: main procedure
  \myproc{$\sys(P, B)$ \label{line:verify-begin}} %
  { % Auxiliary function that recurses on obligations
    \myproc{$\sysaux(S)$ \label{line:aux-begin}}
    { % case split on whether S are inductive:
      \Switch{$\chkinductive{P}{B}(S)$ \label{line:chk-inductive}}{
        % case: invariants are inductive
        \lCase{$\true$:}{\Return{$\true$} \label{line:ret-bound-sat}}
        % case: invariants are not inductive
        \Case{$q$:}{ %
          \Switch{$\tpt(\charbreaks{P}{B}(q))$ \label{line:run-tpt}}{
            % case: has model
            \lCase{$\hasmodel$:}{\Return{$\false$} %
              \label{line:breaks-bound}}
            % case: has no model:
            \lCase{$S_{q}$:}{ %
              \Return{$\sysaux( %
                \mergesums(S, S_{q}))$} \label{line:ret-rec} %
            } %
          } %
        } %
      } %
    } \label{line:aux-end} %
    \Return{$\sysaux((\emptyset, \emptyset))$} %
    \label{line:aux-init} \; %
  } %
  %
  \caption{\sys: a bound-satisfaction verifier for bounds expressed in
    an extension of \lia.  %
    \sys uses procedures $\chkinductive{P}{B}$ (summarized in
    \autoref{sec:verifier}) and $\tpt$, which is presented in detail
    in \autoref{sec:bound-sat-itps}.
    %
  }
  \label{alg:sys}
\end{algorithm}

% Walk through the algorithm, sysaux
\autoref{alg:sys} contains the pseudocode for the core algorithm of
\sys, which takes a program $P$ and bound $B$ and attempts to decide
if $P$ satisfies $B$.
%
To do so, \sys uses a counterexample-guided refinement loop, similar
to conventional automatic safety
verifiers~\cite{henzinger02,henzinger04,mcmillan06}.
%
\sys defines and uses an auxiliary function \sysaux, which takes
visible-suffix summaries $S$ and attempts to construct visible-suffix
summaries over an extension of the visible-suffix summaries of $S$
that are inductive (\autoref{sec:summaries},
\autoref{defn:inductive-path-summaries}) for $P$ and $B$
(\autoref{line:aux-begin}---\autoref{line:aux-end}).
%
\sys runs \sysaux on the empty set of visible-suffix summaries paired
with an empty map to summaries and returns the result
(\autoref{line:aux-init}).

% sysaux to case where bound is satisfied
\sysaux runs a procedure $\chkinductive{P}{B}$ that determines if
visible-suffix summaries $S$ are inductive.
%
If so, $\chkinductive{P}{B}$ returns $\true$, and otherwise returns a
path of $P$ not summarized by $S$ (\autoref{line:chk-inductive};
%
the design of $\chkinductive{P}{B}$ is as described in previous work
on automatic verification of safety properties~\cite{mcmillan06}, and
we omit a description here).
%
If $\chkinductive{P}{B}$ determines that $S$ are inductive summaries,
then $\sysaux$ returns that $P$ satisfies $B$
(\autoref{line:ret-bound-sat}).

% case: bound is not satisfied to subcase: path has model:
Otherwise, if $\chkinductive{P}{B}$ returns a path $q$ not summarized
by $S$, then \sysaux runs a procedure $\charbreaks{P}{B}$ on $q$.
%
$\charbreaks{P}{B}$ returns a tree-interpolation problem for which
each model corresponds to a run of $q$ in $P$ that does not satisfy
$B$, and all tree interpolants correspond to path invariants of $q$
that prove that $q$ satisfies $B$;
%
the design of $\charbreaks{P}{B}$ is described in detail in
\autoref{sec:bound-sat-itps}.
%
\sysaux runs an interpolating theorem prover $\tpt$ for $\mathcal{T}$
on the resulting tree-interpolation problem (\autoref{line:run-tpt}).
%
If $\tpt$ returns that the interpolation problem has a model, then
\sysaux returns that $P$ does not satisfy $B$
(\autoref{line:breaks-bound}).

% subcase: get summaries
Otherwise, if $\tpt$ returns summaries $S_q$ of $q$ that prove that
$q$ satisfies $B$, then \sysaux runs a procedure $\mergesums$ on $S$
and $S_q$ to construct visible-suffix summaries that prove that all
paths of $S$ and $q$ satisfy $B$, recurses on the result, and returns
the result of the recursive call (\autoref{line:ret-rec};
%
the implementation of $\mergesums$ is immediate from previous
approaches for automatically verifying safety
properties~\cite{mcmillan06}).

\subsubsection{Per-Path Bound Satisfaction as Tree-Interpolation}
\label{sec:bound-sat-itps}
%
For program $P \in \lang$ and bound $B \in \tboundctrs$,
$\charbreaks{P}{B}$, given path $q \in \pathsof{P}$, constructs a
$\mathcal{T}$-interpolation problem for which each model corresponds
to a run of $q$ that does not satisfy $B$.
%
To do so, $\charbreaks{P}{B}$ constructs a tree-interpolation problem
(\autoref{sec:itps}, \autoref{defn:tree-itps}) $\ctrtreeof{P}{B}{q} =
(N, E, C)$, defined as follows.
% nodes
\textbf{(1)} The nodes $N$ are the visible suffixes of $q$.
%
I.e., $N = \vissuffixesof{q}$.
% edges:
\textbf{(2)} The edges $E \subseteq N \times N$ are the
immediate-suffix relation over visible suffixes of $q$.
%
I.e.,
\[ E = \setformer{(\cc{L} \cons q', q')}{ %
  \cc{L} \in \locs, q', \cc{L} \cons q' \in \vissuffixesof{q}} \]
% constraints:
\textbf{(3)} The constraints $C$ model the semantics of statements in
$q$ and the condition that $B$ must be satisfied.
% define space of variables
I.e., for each visible suffix $s \in \vissuffixesof{q}$, let there
be a distinct copy of program variables $\vars_s$. Then:
\begin{itemize}
\item
  % branch statements:
  For each branch statement $b \in \brseqs$ such that $\brtargetof{s}
  = \headof{b}$ and $s' = \prelocof{b} \cons s \in \vissuffixesof{q}$,
  %
  let
  \[ C'(s) = \subs{\symrelof{b}}{\vars_{s'}, \vars_{s}} \]
\item
  % matching call and return:
  For call statement $c \in \callseqs$ such that
  $\calltargetof{c} = \headof{s}$ and $s_0 = \prelocof{c} \cons s \in
  \vissuffixesof{q}$ and return statement $r \in \retseqs$ such that
  $s_1 = r :: s \in \vissuffixesof{q}$ and $s_2 \in \vissuffixesof{q}$
  is the maximal extension of $s_1$ in $\vissuffixesof{q}$, let
  %
  \[ C'(s) = \subs{\callsymrel}{\vars_{s_0}, \vars_{s_2}} \land %
  \subs{\retsymrel}{\vars_{s_0}, \vars_{s_1}, \vars_{s}}
  \]
\end{itemize}
%
Then $C([ \finalloc ] ) = C'([ \finalloc]) \land
\replace{B}{\vars'}{\vars_{[ \finalloc ]}}$, and for each $s \not= [
\finalloc ]\in \vissuffixesof{q}$, $C(s) = C'(s)$.
%
Let the space of all summaries over post-state variables defined per
visible suffixes of $q$ be denoted $\suffixsumsof{q} =
\formulas{\bigunion_{s \in \vissuffixesof{q}} \vars_s}$.

% each interpolant of charbreaks is a valid path summary
Each interpolant of $\ctrtreeof{P}{B}{q}$ defines valid visible-suffix
summaries of $q$.
%
In particular, for each map $I: N \to \suffixsumsof{q}$, let $S_I:
\vissuffixesof{q} \to \summaries$ be such that for each $s \in
\vissuffixesof{q}$ with maximal extension $s_0 \in \vissuffixesof{q}$,
%
\[ S_I(s) = \replace{I(s)}{\vars_{s_0}, \vars_s}{\vars, \vars'}
\]
% soundness:
\begin{lemma}
  \label{lemma:char-breaks}
  For each program $P \in \lang$, path $q \in \pathsof{P}$, bound $B
  \in \tboundctrs$, and constraint map $I: \vissuffixesof{q} \to
  \suffixsumsof{q}$, if $I$ are interpolants of $\ctrtreeof{P}{B}{q}$,
  then $S_I$ are valid visible-suffix summaries of $q$
  (\autoref{defn:valid-summaries}).
\end{lemma}
%
A proof of \autoref{lemma:char-breaks} follows immediately from
previous work on tree interpolation~\cite{heizmann10}.
%
\begin{ex}
  \label{ex:tree-itp}
  % problem:
  In \autoref{sec:inferring-invs}, \autoref{fig:bin-search-path}
  depicts a linear tree-interpolation problem $T_q$ for which each
  model defines a run of path $q$ that breaks bound $B$.
  %
  Each node of $T$ is a point in the control path $p$, and each edge
  of $T$ an edge in the path.
  %
  The constraint for each node $n$ is the symbolic transition relation
  of the instruction with destination $n$.
  % solution, interpolants:
  The formulas that label nodes in \autoref{fig:bin-search-path}
  correspond to visible-suffix summaries of $q$ defined by
  interpolants of $T_q$.
\end{ex}

% Give design of theorem prover
\subsection{Tree Interpolation by Theorem Enumeration}
\label{sec:thm-prover}
% Give the interface of the theorem prover:
In this section, we describe the design of a tree-interpolating
theorem prover $\tpt$ for $\mathcal{T}$.
%
In \autoref{sec:core-tp}, we describe the core algorithm of $\tpt$.
%
In \autoref{sec:gen-grounding}, we describe a key procedure used by
$\tpt$ to generate $\mathcal{T}$ formulas to search for a model or
interpolants of a given tree-interpolation problem $T$.

% Give the core theorem-proving algorithm:
\subsubsection{The Theorem-Proving Algorithm}
\label{sec:core-tp}

\begin{algorithm}[t]
  % Declare IO markers.
  \SetKwInOut{Input}{Input}
  %
  \SetKwInOut{Output}{Output}
  % Declare sub-program (procedure) markers.
  \SetKwProg{myproc}{Procedure}{}{}
  % Inputs: a heap program and an error location.
  \Input{A $\mathcal{T}$-tree-interpolation problem $U$.}
  % Output: inductive invariants.
  \Output{Either interpolants for $U$ or $\hasmodel$ to denote that
    $U$ has a $\mathcal{T}$-model.}
  %
  \myproc{$\tpt(U)$ \label{line:tp-begin}} {
    % Auxiliary function that recurses on invariant tree:
    \myproc{$\tptaux(V)$ \label{line:tpaux-begin}}{
      % get a model of the UEF relaxation of B:
      \Switch{$\eufliaissat(V)$ \label{line:is-sat} }{
        % case: unsat
        \lCase{$\none$:}{ %
          \Return{$\eufliaitps(V)$} \label {line:ret-itps}%
        }
        % case: model
        \Case{$m$: \label{line:case-model}}{ %
          % collect unsatisfied clauses,
          $N' \assign \setformer{n}{ %
            n \in \nodesof{U}, m_{\mathcal{T}} \not\tsats C(n')}$ %
          \label{line:unsats} \;
          % if empty, then return that it has a model,
          \lIf{$N' = \emptyset$}{ %
            \Return{$\hasmodel$} \label{line:has-model} %
          } %
          $n' \assign \chooseelt(N')$ \label{line:choose-elt} \;
          %
          $V' = \grndt(\restrict{V}{n'}, m)$ %
          \label{line:call-grndt} \;
          % recurse
          \Return{$\tptaux(\mergetrees(V, V'))$} %
          \label{line:tp-rec} \;
        } %
      } %
    } \label{line:tpaux-end} %
    \Return{$\tptaux(U)$} \label{line:base-call} %
  }
  \caption{$\tpt$: an interpolating theorem prover for
    $\mathcal{T}$.}
\label{alg:tpt}
\end{algorithm}

% Walk through the algorithm:
\autoref{alg:tpt} contains pseudocode for $\tpt$, an interpolating
theorem prover for $\mathcal{T}$.
%
$\tpt$ takes a $\mathcal{T}$-interpolation problem $U$ and returns
either the value $\hasmodel$ to denote that $U$ has a
$\mathcal{T}$-model or interpolants for $U$.
%
$\tpt$ defines a procedure $\tptaux$
(\autoref{line:tpaux-begin}---\autoref{line:tpaux-end}) that takes a
$\mathcal{T}$-tree-interpolation problem $V$ that is as weak as $U$
and returns either $\hasmodel$ if $V$ has a model or
$\mathcal{T}$-interpolants of $V$.
%
$\tpt$ calls $\tptaux$ on $U$ and returns the result
(\autoref{line:base-call}).

% Walk through the aux procedure: case, has no model.
$\tptaux$ determines if $V$ has an \euflia model by running
$\eufliaissat$ on $V$ (\autoref{line:is-sat};
%
$\eufliaissat$ is introduced in \autoref{sec:logic}).
%
If $\eufliaissat$ returns that $V$ has no \euflia model, then
$\tptaux$ runs an interpolating theorem prover $\eufliaitps$
(introduced in \autoref{sec:itps}) on $V$ and returns the resulting
interpolants (\autoref{line:ret-itps}).

% case: has a model
Otherwise, if $\tptaux$ returns an \euflia model $m$ of $V$
(\autoref{line:case-model}), then $\tptaux$ collects the nodes $N'
\subseteq \nodesof{V}$ whose clauses are not satisfied by
$m_{\mathcal{T}}$, the restriction of $m$ to the variables that occur
in $V$ combined with the standard model of $\mathcal{T}$
(\autoref{line:unsats}).
%
If $N'$ is empty (i.e., $m_{\mathcal{T}}$ satisfies $U$), then
$\tptaux$ returns that $V$ has a model (\autoref{line:has-model}).
%
Otherwise, $\tptaux$ constructs a tree-interpolation problem $V'$ that
is as weak as $V$ but for which $m$ is not an \euflia model by running
the procedure $\grndt$ on $\restrict{V}{n'}$ and $m$
(\autoref{line:call-grndt}; $\grndt$ is described in
\autoref{sec:gen-grounding}).
%
$\tptaux$ merges $V$ with $V'$, recurses on the result, and returns
the result of the recursion (\autoref{line:tp-rec}).

% tpt:
\begin{ex}
  \label{ex:tpt}
  \autoref{sec:inferring-invs} describes a run of $\tpt$ on the
  tree-interpolation problem for path $q$ of program
  \cc{BinarySearch}.
  %
  $\tpt$ first obtains a model $m$ of the tree-interpolation problem
  for $q$, $T_q$, under \euflia (\autoref{alg:tpt},
  \autoref{line:is-sat}).
  %
  $\tpt$ determines that $m_{\mathcal{T}}$ does not satisfy the clause
  for the node $g$ of \autoref{fig:bin-search-path} under the standard
  model of non-linear arithmetic (\autoref{line:unsats}), chooses $g$
  as the unique node in $T_q$ that is not satisfied by
  $m_{\mathcal{T}}$, and runs $\grndt$ on $T_q$ restricted to $g$
  (i.e., $T_q$ itself).
  %
  $\grndt$ generates a weaker tree-interpolation problem described in
  \autoref{sec:gen-grounding}, \autoref{ex:grounding}, which $\tptaux$
  merges with $T_p$, and recurses on (\autoref{line:tp-rec}).

  % last iteration:
  The tree-interpolation problem generated by $\grndt$ is sufficiently
  weak that it is unsatisfiable under \euflia.
  %
  Thus, $\tptaux$ returns interpolants for it found by $\eufliaitps$.
\end{ex}

% walk through procedure for generating grounded axioms:
\subsubsection{Enumerating Quantifier-Free $\mathcal{T}$-Theorems}
\label{sec:gen-grounding}

% interface:
\begin{algorithm}[t]
  % Declare IO markers.
  \SetKwInOut{Input}{Input}
  %
  \SetKwInOut{Output}{Output}
  % Declare sub-program (procedure) markers.
  \SetKwProg{myproc}{Procedure}{}{}
  % Inputs: a heap program and an error location.
  \Input{A $\mathcal{T}$-interpolation problem $U$ over variables $X$
    and model $m$ that satisfies $U$ under $\euflia$.}
  % Output: inductive invariants.
  \Output{A $\mathcal{T}$-interpolation problem that is as weak as $U$
    but is not satisfied by $m$ under $\euflia$. }
  %
  \myproc{$\grndt(U, m)$ \label{line:grndt-begin}} {
    % compute sets of co-located variables,
    $\mathcal{V} \assign \colocitp(U)$ \label{line:coloc-itp} \;
    % compute blocking clause
    $E \assign \blocking(\ctrsof{T}(\headof{U}))$ %
    \label{line:block} \;
    % Auxiliary function that recurses on set of grounded axioms:
    \myproc{$\grndtaux(A)$ \label{line:grndt-aux-begin}}{
      % filter down to the set of axioms with colocated variables,
      $A_{\mathcal{V}} = %
      \setformer{a}{ %
        n \in \nodesof{U}, a \in A, %
        \vocab(a) \subseteq \mathcal{V}(n)}$ %
      \label{line:coloc} \;
      % test if combination is satisfiable,
      \uIf{$\issat( %
        \ctrof{U} \land E \land \bigland A_{\mathcal{V}})$ %
        \label{line:grndt-sat}}{
        \Return{$\grndtaux(\add{A}{\tenum{X}(A)})$} %
        \label{line:grndt-recurse} %
      } %
      \Else{ %
        $A_{\mathcal{V}}' \assign %
        \minimal(A_{\mathcal{V}}, \ctrof{U} \land B)$ %
        \label{line:minimize} \;
        % map node to conjunction of constraints with its vocab,
        $L(n) \assign %
        \bigland \setformer{a}{
          a \in A_{\mathcal{V}}', \vocab(a) \subseteq \mathcal{V}(n)}$ %
        \label{line:node-to-forms} \;
        % combine the invariants:
        \Return{$U\ \mathsf{with}\ C(n) \assign \ctrof{U}(n) \land L(n)$} \;
      } \label{line:grndt-aux-end}
    } %
    \Return{$\grndtaux(\emptyset)$} %
    \label{line:grndt-base-call} \; %
  }
  \caption{$\grndt$: takes a $\mathcal{T}$-tree-interpolation problem
    $U$ and assignment $m$ of the variables of $U$ that satisfies $U$
    under \euflia, and generates a tree-interpolation problem as weak
    as $U$ that $m$ does not satisfy under $\euflia$.}
\label{alg:grnd}
\end{algorithm}

% interface:
% walk through grounding: interface, prelude:
\autoref{alg:grnd} contains pseudocode for $\grndt$
(\autoref{line:grndt-begin}---\autoref{line:grndt-base-call}).
%
$\grndt$ takes a $\mathcal{T}$-interpolation problem $U \in
\itpproblems{\mathcal{T}}{X}$ over variables $X$ and assignment $m: X
\to \domainof{\mathcal{T}}$ and returns a $\mathcal{T}$
tree-interpolation problem that is as weak as $U$ but for which $m$ is
not a model under $\euflia$.
%
$\grndt$ uses a procedure $\tenum{X}$ that takes quantifier-free
$\mathcal{T}$ formulas $A$ over variables $X$ and enumerates a
quantifier-free $\mathcal{T}$ formula over $X$ not in $A$.

% grndt: walk through the procedure:
$\grndt$ constructs a map $\mathcal{V}: \nodesof{U} \to \pset(X)$ from
each node $n \in \nodesof{U}$ to the vocabulary of $n$
(\autoref{line:coloc-itp}; the vocabulary of a node in an
interpolation problem is defined in \autoref{sec:itps},
\autoref{defn:tree-itps}).
%
$\grndt$ then runs a procedure $\blocking$ that takes an assignment $m: X
\to \values$ and returns a constraint that each variable in $X$ equal
to its value under $m$ (\autoref{line:block}).
%
I.e.,
\begin{align*}
  \blocking(m) \equiv %
  \bigland_{x \in X} \elts{ x = \termof{m(x)} }
\end{align*}
%
$\grndt$ sets $E$ to the result of running \blocking on $m$.
% example: blocking clause
\begin{ex}
  \label{ex:blocking-clause}
  When $\tpt$ is run to find path invariants of path $q$ of
  \cc{BinarySearch} (\autoref{sec:inferring-invs}), it enumerates
  non-linear theorems until it finds a set that is not satisfied by
  the model $m$, which assigns $\cost_1$ to $2$, %
  $\cc{len}(\cc{arr})$ to $4$.
  %
  When $\tpt$ runs $\grndt$, \blocking generates clause
  $\cost_1 = 2 \land \cc{len}(\cc{arr}) = 4$.
\end{ex}

% aux procedure:
$\grndt$ defines a procedure $\grndtaux$
(\autoref{line:grndt-aux-begin}---\autoref{line:grndt-aux-end}) that
uses a set of quantifier-free $\mathcal{T}$ theorems to find a
$\mathcal{T}$-tree interpolation problem that is as weak as $U$ but is
not satisfied by $m$ under \euflia.
% aux procedure:
$\grndtaux$, given quantifier-free formulas $A$, first collects the
set $A_{\mathcal{V}}$ of formulas in $A$ that each have a vocabulary
contained by the vocabulary of some set in $\mathcal{V}$
(\autoref{line:coloc}).
%
$\grndtaux$ then checks if there is a model of the constraints of $U$,
$E$, and all theorems in $A_{\mathcal{V}}$ (\autoref{line:grndt-sat}).
%
If so, then $\grndtaux$ recurses on $A$ extended with the next
enumerated theorem of $\mathcal{T}$ (\autoref{line:grndt-recurse}).

% case: formulas aren't mutually satisfiable
Otherwise, if $\ctrof{U}$, $E$, and $A_{\mathcal{V}}$ are not mutually
satisfiable, then $\grndtaux$ constructs a minimal subset
$A_{\mathcal{V}}'$ of $A_{\mathcal{V}}$ that is mutually unsatisfiable
with $\ctrof{U}$ and $E$ (\autoref{line:minimize}).
%
$\grndtaux$ returns the given tree-interpolation problem $U$, updated
with a constraint map $C'$ that maps each node $n$ of $U$ to the
conjunction of its constraint in $U$ and the conjunction of all
theorems in $A_{\mathcal{V}}'$ whose vocabularies are contained in the
set of variables in the vocabulary of $n$
(\autoref{line:node-to-forms}---\autoref{line:grndt-aux-end}).

% walk through enumeration:
\begin{ex}
  \label{ex:grounding}
  When $\tpt$ runs $\grndt$ on the tree-interpolation problem
  $\ctrtreeof{\cc{BinarySearch}}{\binsearchbound}{q}$ with variables
  $X$ for the path $q$ of \cc{BinarySearch}
  (\autoref{sec:inferring-invs}), $\grndt$ constructs the
  evaluation clause $E$ (\autoref{ex:blocking-clause}) and runs
  $\grndtaux$.
  %
  For $\mathcal{T}$ the theory of non-linear arithmetic, $\grndtaux$
  iteratively runs $\tenum{X}$, which generates $\mathcal{T}$ formulas
  over $X$, such as \autoref{thm:ax-log-2-q},
  \autoref{thm:ax-log-sub1}, and $\log{(\cc{fst}_0 \cdot \cc{fst}_1)}
  = \log{\cc{fst}_0} + \log{\cc{fst}_1}$.

  % filtered theorems:
  Of the enumerated theorems, $A_{\mathcal{V}}$ (defined at
  \autoref{alg:grnd}), \autoref{line:coloc}) contains, e.g.,
  \autoref{thm:ax-log-sub1} because $\cc{lst}_0$ and $\cc{fst}_0$ are
  in the vocabulary of some node in
  $\ctrtreeof{\cc{BinarySearch}}{\binsearchbound}{q}$, namely the
  nodes corresponding to points $b$ and $c$ in $q$ (depicted in
  \autoref{sec:inferring-invs}, \autoref{fig:bin-search-path}).
  %
  $A_{\mathcal{V}}$ does not contain, e.g., $\log{(\cc{fst}_0 \cdot
    \cc{fst}_1)} = \log{\cc{fst}_0} + \log{\cc{fst}_1}$ because
  $\cc{fst}_0$ and $\cc{fst}_1$ are not in the vocabulary of any node
  in $\ctrtreeof{\cc{BinarySearch}}{\binsearchbound}{q}$.

  % final condition: enumerate a sufficient set
  $\grndt$ enumerates theorems of non-linear arithmetic over $X$ until
  it enumerates a set that is mutually unsatisfiable with
  $\ctrtreeof{\cc{BinarySearch}}{\binsearchbound}{q}$ (given in
  \autoref{sec:inferring-invs}) and $E$.
  %
  One such set are the theorems
  \autoref{thm:ax-log-2-q}---\autoref{thm:ax-log-sub2}, given in
  \autoref{sec:inferring-invs}.
\end{ex}

% grounding generates weaker tree-interpolation problems
When $\grndt$ returns, it always generates a tree-interpolation
problem that is as weak as its input.
%
\begin{lemma}
  \label{lemma:grounding}
  For all variables $X$, each tree-interpolation problems $U \in
  \itpproblems{\mathcal{T}}{X}$, and assignment $m$ over $X$, if
  $\grndt$ terminates on $U$ and $m$, then $\grndt(U, m)$ is as weak
  as $U$.
\end{lemma}
%
\begin{proof}
  % A only contains T-theorems (induction)
  Proof by induction on the evaluation of $\grndtaux$ on its argument
  $A$.
  %
  The claim proved by induction is that each formula in $A$ is a
  theorem of $\mathcal{T}$.
  %
  For the base step, $\grndtaux$ is initially called on the empty set
  of theorems (\autoref{alg:grnd}, \autoref{line:grndt-base-call}),
  and the claim is trivially satisfied.
  %
  For the inductive step, by the inductive hypothesis, $A$ contains
  only $\mathcal{T}$-theorems.
  %
  In the next step of evaluation, $\grndtaux$ is called with a set
  consisting of $A$ and a formula generated by $\tenum{X}$, which by
  assumption generates only $\mathcal{T}$ theorems
  (\autoref{sec:logic}).
  %
  Thus, in the next step of evaluation of $\grndtaux$, $A$ contains
  only $\mathcal{T}$ theorems.

  % as-weak-as condition 1: conjunction of formulas is weaker:
  $\grndtaux$ returns a tree-interpolation problem with nodes and
  edges identical to $U$, and a constraint map $C'$ that maps each
  node $n$ to a conjunction of its constraint in $U$ and formulas in
  $A$.
  %
  The fact that each formula in $A$ is a $\mathcal{T}$ theorem implies
  that $n$ is bound to a constraint that is no stronger under
  $\mathcal{T}$ then its constraint in $U$.

  % as-weak-as condition 2: identical constraint vocabs
  $\grndtaux$ includes a formula in $A$ in the constraint $C'(n)$ only
  if its vocabulary is a subset of the vocabulary for $n$
  (\autoref{line:coloc} and \autoref{line:node-to-forms}).
  %
  Thus the vocabulary of $C'(n)$ the vocabulary of $\ctrsof{T}(n)$.
  %
  $U'$ as weak as $U$, by definition (\autoref{sec:logic},
  \autoref{defn:weakening}).
\end{proof}
% extra facts:
$T$ is as weak as $T'$ as well, although this fact is both trivial
from the construction of $T'$ from $T$, and not required to prove the
soundness of \sys.
%
Furthermore, $\grndt$, given tree-interpolation problem $T$ and model
$m$, always generates a tree-interpolation problem for which $m$ is
not a model.
%
However, this fact is not required in order to prove the soundness of
\sys (\autoref{sec:correctness}, \autoref{thm:soundness}), so we
withhold a complete statement and proof.

% tpt is a sound interpolating theorem prover
A consequence of \autoref{lemma:grounding} is that $\tpt$ is a sound
interpolating theorem prover for $\mathcal{T}$.

%
\begin{lemma}
  \label{lemma:tpt}
  $\tpt$ is a sound interpolating theorem prover for $\mathcal{T}$
  (\autoref{sec:logic}, \autoref{defn:itp-prover}).
\end{lemma}
%
\begin{proof}
  For $\mathcal{T}$-tree-interpolation problem $U$, proof by induction
  on the evaluation of $\tpt(U)$.
  %
  The claim established by induction is that $V$ (\autoref{alg:grnd},
  \autoref{line:tpaux-begin}) is as weak as $T$.
  % base step:
  For the base step of the claim's proof, $U$ is set to $T$ at
  \autoref{line:base-call}.
  %
  For the inductive step of the claim's proof, by the inductive
  hypothesis, $U$ is as weak as $T$.
  %
  By \autoref{lemma:grounding}, $\grndt(\restrict{U}{n'}, m)$ is as
  weak as $\restrict{U}{n'}$, and by the definition of $\mergetrees$
  (\autoref{sec:itps}), $U' = \mergetrees(U, \grndt(\restrict{U}{n'},
  m))$ is as weak as $U$.
  %
  Thus, in \sysaux's next step of evaluation, $U$ is as weak as $T$.

  % prove goal:
  $\tpt$ only returns a constraint map if it represents interpolants
  of $U$ under \euflia (\autoref{line:ret-itps}).
  %
  Each interpolant of $U$ under \euflia is an interpolant of $U$ under
  $\mathcal{T}$.
  %
  The fact that $U$ is weaker than $T$, combined with
  \autoref{lemma:weak-itps} (see \autoref{sec:logic}), implies that if
  $\tpt$ returns a constraint map $I$, then $I$ are valid interpolants
  of $T$.
\end{proof}
%
Because $\tpt$ is a valid decision procedure for theories that may not
be decidable, $\tpt$ is not total: i.e., there are tree-interpolation
problems on which it may not terminate.

% State correctness of the bound-satisfaction algorithm.
\subsection{Properties}
\label{sec:correctness}
%
\sys is a sound verifier for the bound-satisfaction problem.
%
\begin{thm}
  \label{thm:soundness}
  %
  For each program $P \in \lang$ and bound $B \in \tboundctrs$, if
  $\sys(P, B) = \true$, then $P \sats B$.
\end{thm}
%
\begin{proof}
  Proof by induction on the evaluation of \sys on $P$ and $B$.
  %
  The claim proved by induction is that at each step of the evaluation
  of \sys, the visible suffix summaries $S$ are valid
  (\autoref{sec:summaries}, \autoref{defn:valid-summaries}).
  % base step:
  The base step of the proof of the claim follows from the definition
  of valid summaries and the fact that \sysaux is called initially
  with summaries over an empty set of suffixes
  (\autoref{sec:verifier}, \autoref{alg:sys},
  \autoref{line:aux-init}).

  % prove the inductive step:
  The inductive step of the proof of the claim follows from the
  following argument.
  %
  By the fact that each interpolant of $\charbreaks{P}{B}(q)$ is a
  valid summary that $q$ satisfies $B$ (\autoref{sec:bound-sat-itps},
  \autoref{lemma:char-breaks}) and the fact that if $\tpt$ generates a
  constraint map then the map is a valid interpolant of its input
  (\autoref{sec:gen-grounding}, \autoref{lemma:tpt}), $S_q$ are valid
  $\euflia \union \mathcal{T}$ summaries that prove that $q$ satisfies
  $B$.
  %
  The fact that $S_q$ combined with the assumption that \mergesums,
  given valid a pair of valid summaries, generates valid summaries
  (\autoref{sec:verifier}), implies that \sysaux is called on valid
  suffix summaries in its next iteration.

  % get the final result:
  The claim, combined with the fact that \sysaux only returns true if
  $\chkinductive{P}{B}$ is run on $S$ and returns $\true$, and the
  assumption that $\chkinductive{P}{B}$ when given valid path summaries
  $S$ returns $\true$ only if $S$ are inductive path summaries,
  implies that $\sys(P, B) = \true$ only if $P$ has inductive
  summaries.
  %
  The fact that inductive summaries are evidence of bound satisfaction
  (\autoref{sec:summaries}, \autoref{lemma:bound-sat-evidence})
  implies that $P \sats B$.
\end{proof}

%
% generating counterexamples:
In \autoref{sec:verifier}, we presented \sys as taking a program $P$
and bound $B$ and returning a Boolean value denoting if $P$ satisfies
$B$.
%
\sys can be directly extended so that if it determines that $P$ does
not satisfy $B$, then it returns a run of $P$ on which it does not
satisfy $B$.
%
In particular, the theorem prover $\tpt$ (\autoref{sec:thm-prover})
can be extended so that if it finds an assignment to logical
variables that satisfy the path formula of a path $q$ under the
standard model of $\mathcal{T}$, then $\tpt$ returns the satisfying
model.
%
\sys can be extended to take such a model $m$ from $\tpt$ and from it,
synthesize a run of path $q$ that does not satisfy $B$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
