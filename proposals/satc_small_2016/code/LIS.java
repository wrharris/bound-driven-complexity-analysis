public int lengthOfLIS(int[] nums, int n) {
  if (nums == null || n == 0) return 0;
  int[] res = new int[n];
  int len = 0;
  res[len] = nums[0];
  len++;
  for (int i = 1; i < n; i++) {
    if (nums[i] < res[0]) res[0] = nums[i]
    else if (nums[i] > res[len - 1]) {
      res[len] = nums[i];
      len++; } 
    else {
      int j = n - 1;
      while (i < j) {
        int mid = (i + j) / 2;
        if (res[mid] < nums[i]) i = mid + 1;
        else j = mid; }
      res[j] = nums[i]; } }
  return len; }
