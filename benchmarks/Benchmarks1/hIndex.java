//7. O(n)  - 
public int hIndex(int[] citations) {
    int size = citations.length;
    int count[] = new int[size + 1];
    for(int i = 0; i < size; i++){
        if (citations[i] >= size) {
            count[size]++;
        }else{
            count[citations[i]]++;
        }
    }

    for(int i = size - 1; i > 0; i--){
        count[i] += count[i+1];
    }

    for (int i = size; i > 0; i--) {
        if (count[i] >= i) {
            return i;
        }
    }

    return 0;
}