package samples.quadratic;

import toy_benchmark.ErrorFunction;

public class JavaDPSolution {

	public static void main(String args[]) {

		JavaDPSolution jdps = new JavaDPSolution();
		int[] coins = { 1, 2, 3, 4 };
		System.out.println(jdps.coinChange(coins, 5));

	}

	public int coinChange(int[] coins, int amount) {
		int[] table = new int[amount + 1];
		for (int i = 0; i < amount + 1; i++)
			table[i] = -1;
		table[0] = 0;
		for (int i = 1; i < amount + 1; i++) {
			for (int j = 0; j < coins.length; j++) {
				if (i - coins[j] >= 0 && table[i - coins[j]] > -1
						&& (table[i] == -1 || table[i] > (table[i - coins[j]] + 1))) {
					table[i] = table[i - coins[j]] + 1;
				}
			}
		}
		ErrorFunction.Error();
		return table[amount];
	}
}
