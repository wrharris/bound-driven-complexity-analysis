1. O(n) - simple algo finding max profit/ can be used with grades or a combination of this and one of the below algos
public class Solution {
    public int maxProfit(int[] prices) {
        if (prices.length == 0)
        {
            return 0;
        }
        int max = 0, min = prices[0];
        int profit = 0;

        for (int i = 1; i < prices.length; i++)
        {
            if (prices[i] < min)
            {

                min = prices[i];
            }
            else
            {
                if (prices[i] - min > profit)
                {
                    profit = prices[i] - min;
                }
            }
         }
     return profit;

    }
}
2. O(n) - ratings on candy can relate to grading since they are being compared
public int candy(int[] ratings) {
  if (ratings == null || ratings.length == 0) return 0;

  int n = ratings.length;

  // a[i] stores the num of candies of i-th kid
  int[] a = new int[n]; a[0] = 1;

  // scan from left to right
  for (int i = 1; i < n; i++)
    a[i] = (ratings[i] > ratings[i - 1]) ? a[i - 1] + 1 : 1;

  // scan from right to left
  int sum = a[n - 1];

  for (int i = n - 2; i >= 0; i--) {
    if (ratings[i] > ratings[i + 1])
      a[i] = Math.max(a[i], a[i + 1] + 1);

    sum += a[i];
  }
  return sum;
}

3. O(n) - very simple starts off with count and increments
public int climbStairs(int n) {
    if(n==1||n==0)
        return n;
    int count1=1;
    int count2=1;
    for(int i=2; i<=n; i++){
        int temp = count2;
        count2 = temp+count1;
        count1 = temp;
    }
    return count2;
}

// AP: missing method header.
 4. O(n^2) - uses this with multiple comparisons 
 int[] table = new int[amount+1];
    for (int i = 0; i < amount + 1; i++)
        table[i]  = -1;
    table[0] = 0;
    for (int i = 1; i < amount + 1; i++){
        for (int j = 0; j < coins.length; j++){
            if (i - coins[j] >= 0 && table[i-coins[j]] > -1 && (table[i] == -1 || table[i] > (table[i - coins[j]] + 1))){
                table[i] = table[i - coins[j]] + 1;
            }
        }
    }
    return table[amount];
}

// 5. 
/* O(n) - simple solution/ short could possibly use this  
 * 
 * KEY: iteration isn't simple enumration.
 *
 * AP: isn't this O(log n)?
 */
 public int findPeakElement(int[] a) {
        int low = 0, mid = 0, high = a.length - 1;
        while(low < high) {
            mid = low + (high-low)/2;
            if(a[mid] < a[mid+1]) low = mid+1;
            else high = mid;
        }
        return low;
    }

6. O(n) - simple solution. could possibly implement this
class Solution {
public:
    int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
        int start(0),total(0),tank(0);
        //if car fails at 'start', record the next station
        for(int i=0;i<gas.size();i++) if((tank=tank+gas[i]-cost[i])<0) {start=i+1;total+=tank;tank=0;}
        return (total+tank<0)? -1:start;
    }
};
7. O(n)  - 
public int hIndex(int[] citations) {
    int size = citations.length;
    int count[] = new int[size + 1];
    for(int i = 0; i < size; i++){
        if (citations[i] >= size) {
            count[size]++;
        }else{
            count[citations[i]]++;
        }
    }

    for(int i = size - 1; i > 0; i--){
        count[i] += count[i+1];
    }

    for (int i = size; i > 0; i--) {
        if (count[i] >= i) {
            return i;
        }
    }

    return 0;
}
8.O(n) uses dynamic programming
public int rob(int[] nums) {
    int len = nums.length;
    if(len == 0) return 0;
    if(len == 1) return nums[0];
    int[] val = new int[len];
    val[0] = nums[0];
    val[1] = Math.max(nums[0], nums[1]);
    for(int i=2; i<len; i++)
        val[i] = Math.max(nums[i] + val[i-2], val[i-1]);
    return val[len-1];
}
9. O(n)
public int jump(int[] A) {
    int sc = 0;
    int e = 0;
    int max = 0;
    for(int i=0; i<A.length-1; i++) {
        max = Math.max(max, i+A[i]);
        if( i == e ) {
            sc++;
            e = max;
        } 
    }
    return sc;
}
10. dynamic programming/ O(n^2)
public int lengthOfLIS(int[] nums) {
  if (nums.length == 0) return 0;

  int n = nums.length, max = 0;
  int[] dp = new int[n];

  for (int i = 0; i < n; i++) {
    dp[i] = 1;

    for (int j = 0; j < i; j++) {
      if (nums[i] > nums[j] && dp[j] + 1 > dp[i]) {
        dp[i] = dp[j] + 1;
      }
    }

    max = Math.max(max, dp[i]);
  }

  return max;
}
11. This is a simple solution as well: O(n)
public static int maxSubArray(int[] A) {
    int maxSoFar=A[0], maxEndingHere=A[0];
    for (int i=1;i<A.length;++i){
        maxEndingHere= Math.max(maxEndingHere+A[i],A[i]);
        maxSoFar=Math.max(maxSoFar, maxEndingHere); 
    }
    return maxSoFar;
}

/* 12. O(n) solution/ this solution has many inputs but still simple
 *
 * KEY: non-trivial ranking argument.
 */
public class Solution {

    public int minSubArrayLen(int targetSum, int[] nums) {
        int minLength = Integer.MAX_VALUE;

        int left=0, right=0;
        int slidingSum = 0;
        int n = nums.length;

        while(right < n) {
            if(slidingSum  + nums[right] < targetSum) {
                slidingSum += nums[right];
                right += 1;
            } else {
                minLength = Math.min(minLength, right - left + 1);
                slidingSum -= nums[left];
                left += 1;
            }
        }

        return minLength == Integer.MAX_VALUE ? 0 : minLength;
    }
}

13.  O(n) time and O(1) space/ simple solution that may be usable 
	int n = nums.length;
    int original = 0;
    int current = 0;

    for(int i=0;i<nums.length;i++){
        original+=i;
        current+=nums[i];
    }

    return original+n-current;
}
14. O(n) once again but also simple and can be implemented by slightly changing it to a bit more complex to fit our contraints
public class Solution {    

    public void moveZeroes(int[] nums) {
        int z = -1;
        for (int i=0; i< nums.length; i++) {
            int temp = nums[i];
            if (temp != 0) {
                nums[i]=nums[++z];
                nums[z]=temp;
            }
        }
    }

}

15. O(n) similar to #14
public class Solution
{
    public int removeDuplicates(int[] nums)
    {
        int dupes = 0;

        for (int i = 1; i < nums.length; i++)
        {
            if (nums[i] == nums[i - 1])
                dupes++;

            nums[i - dupes] = nums[i];
        }

        return nums.length - dupes;
    }
}


/* 16.O(n) two if contraints and a simple while loop
 * 
 * KEY: non-trivial bound on loop iterations.
 */
public int removeElement(int[] nums, int val) {
    if(nums.length == 0) return 0;
    if(nums.length == 1) return nums[0] == val ? 0 : 1;
    int i=0, j=nums.length-1;
    while(i <= j) {
        if(nums[j] == val) j--;
        else {
            if(nums[i] == val) {
                nums[i] = nums[j];
                j--;
                i++;
            } else {
                i++;
            }
        }
    }
    return i;
}

/* 17.  O(n) while loop, may be useful depending on what exactly were looking at
 *
 * KEY: non-trivial termination.
 *
 * AP: isn't this O(log n)?
 */
public int searchInsert(int[] A, int target) {
        int low = 0, high = A.length-1;
        while(low<=high){
            int mid = (low+high)/2;
            if(A[mid] == target) return mid;
            else if(A[mid] > target) high = mid-1;
            else low = mid+1;
        }
        return low;
    }

 18. dynamic Programing with O(n^2) solution
 This can be implemented or a combination of any of the set of above compliexities that are O(n) for a more complex algorithm
 public class Solution {
    public int uniquePaths(int m, int n) {
        Integer[][] map = new Integer[m][n];
        for(int i = 0; i<m;i++){
            map[i][0] = 1;
        }
        for(int j= 0;j<n;j++){
            map[0][j]=1;
        }
        for(int i = 1;i<m;i++){
            for(int j = 1;j<n;j++){
                map[i][j] = map[i-1][j]+map[i][j-1];
            }
        }
        return map[m-1][n-1];
    }
}
