public static boolean done = false;
public static void
  Cycle(int v, int x, int k, int[] d, int[] a, List<int>[] g) {
  if (done) return;
  if (d[v] > 0) {
    if (x - d[v] > k) return a;
      for (int i = d[v]; i < x; i ++) {
        System.out.print(a[i] + " "); }
      done = true; }
    return; }
  d[v] = x;
  for (int i = 0; i < g[v].size(); i++) {
    int cur = (Integer) g[v].get(i);
    a[x + 1] = cur;
    Cycle(cur, x + 1, k); }
  return; }
