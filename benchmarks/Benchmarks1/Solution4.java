
//13.  O(n) time and O(1) space/ simple solution that may be usable 
Public Class Solution4 {
	int n = nums.length;
    int original = 0;
    int current = 0;

    for(int i=0;i<nums.length;i++){
        original+=i;
        current+=nums[i];
    }

    return original+n-current;
}
}