package samples.quadratic;

import toy_benchmark.ErrorFunction;

public class On2DPSolution2 {
	public static void main(String args[]) {

		On2DPSolution2 on22 = new On2DPSolution2();
		int[] nums = { 1, 2, 3, 4, 5 };
		System.out.println(on22.lengthOfLIS(nums));

	}

	public int lengthOfLIS(int[] nums) {
		if (nums.length == 0)
			return 0;

		int n = nums.length, max = 0;
		int[] dp = new int[n];

		for (int i = 0; i < n; i++) {
			dp[i] = 1;

			for (int j = 0; j < i; j++) {
				if (nums[i] > nums[j] && dp[j] + 1 > dp[i]) {
					dp[i] = dp[j] + 1;
				}
			}

			max = Math.max(max, dp[i]);
		}
		ErrorFunction.Error();
		return max;
	}
}
