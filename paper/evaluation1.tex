\section{Empirical Evaluation}
\label{sec:evaluation}
% Overview of questions.
We performed an empirical evaluation of our bound-satisfaction
analysis.
%
The objective of our evaluation was to answer the following questions:
\begin{enumerate}
\item 
  Is the bound-satisfaction analysis expressive in practice?
  %
  I.e., can it prove that modules of practical program satisfy
  expected bounds, which in practice are typically non-linear?
\item 
  % Is the analysis efficient?
  Is the bound-satisfaction analysis efficient?
  %
  I.e., when it proves that program module satisfies a bound, or finds
  an input which the module does not satisfy the bound, does it do so
  efficiently enough to be potentially useful for a program developer?
  %
  In particular, does the lazy-embedding technique enable the analysis
  to find invariants that allow it to find a valid proof quickly?
\end{enumerate}

% Overview of what we did to answer the questions.
To answer the above questions, we collected a set of benchmarks from
the Java Platform Libraries, and derived expected bounds for several
library methods.
%
We implemented our bound-satisfaction analysis as a tool--called
\sys--and applied \sys to each benchmark method and its expected
bound.
%
In summary, we found that \sys was able to prove that methods from a
variety of libraries satisfied their expected bounds.
%
Furthermore, \sys did so efficiently enough that it could feasibly
be used in an edit-compile-run cycle when analyzing small program
modules, and as part of a testing suite for code review for larger
modules.

% Give an overview of the rest of the evaluation section.
The remainder of this section describes our results in detail.
%
In \autoref{sec:implementation}, we describe the implementation of our
analysis.
%
In \autoref{sec:benchmarks}, we describe the benchmarks on which we
evaluated our analysis.
%
In \autoref{sec:data}, we present the results of our evaluation, and
draw feasible conclusions concerning our analysis.

\subsection{Implementation}
\label{sec:implementation}
%
Our implementation \sys is an extension of the analysis described in
\autoref{sec:analysis}, in that when it completes analyzing a program
it outputs either a step-count forest that proves that the program
always satisfies an expected bound, or a program run, represented as a
sequence of program states, on which the program does not satisfy its
expected bound.
%
\sys the Joeq~\cite{Joeq} program-analysis framework to construct the
control-flow graph of a program.
%
\sys uses the Z3 automatic theorem prover~\cite{demoura08} to
determine the satisfiability of formulas constructed throughout the
analysis and to synthesize interpolants that are used as invariants
prove the correctness of individual program paths.
%
In \autoref{sec:analysis}, we described an analysis for a simple
language of programs that operate over only bounded integers.
%
\sys can accurately analyze programs that use other standard features
of the Java Bytecode language, including arrays and references.
%
The semantics of these operations is encoded using various theories
supported by Z3, namely the theories of uninterpreted functions with
equality and the theory of arrays.
%
\begin{comment}
  % TODO: check with Akhilesh on this
  we have modified/removed certain features in the benchmarks which
  contain instruction semantics which aren't currently supported in
  our analysis so as to verify our results.
\end{comment}

% Benchmarks:
\subsection{Benchmarks}
\label{subsec:Benchmarks}
%
In this section, we describe the benchmarks that we collected from the
from the Java Platform Library. \cite{javalibs}
%
For each benchmark, we derived a bound that we expected the program to
satisfy by inspection of the program source code and documentation.

% Arrays.
\paragraph{Arrays}
\texttt{Arrays} contains the Java Platform's implementation of an
array datatype.
%
We applied our analysis to verify that the \texttt{mergesort} runs
within a quadratic over-approximation of expected performance of the
merge-sort algorithm.
%
Note that because our analysis verifies programs using bounded
arithmetic and generates invariants by embedding bounded arithmetic in
the theory of uninterpreted functions, it can in principle be applied
to check that programs satisfy logarithmic bounds in addition to
polynomial bounds.
%
The absence of support for logarithmic bounds is only a limitation of
our current prototype.

\paragraph{BitSieve}
BitSieve implements algorithm that finds prime-number candidates.
%
We verified that the method \texttt{retrieve}, used to test for
probable primes and return successful candidates, runs in quadratic
time.

\paragraph{BitSet}
%
\texttt{BitSet} implements a vector of bits that grows on demand, and
illustrates an interesting use of \sys.
%
Initially, we were not able to derive an expected bound for
\texttt{BitSet} with confidence, and so we initially analyzed
\texttt{BitSet} to determine if its methods \texttt{flip} and
\texttt{set} satisfy quadratic bound.
%
After \sys verified that both methods do indeed satisfy such bounds,
we reapplied \sys to determine if \texttt{flip} satisfies a linear
bound, and \sys determined that both methods satisfy the tighter
linear bounds as well.

\paragraph{ByteArrayInputStream}
\texttt{ByteArrayInputStream} maintains a buffer that can be accessed
by methods for reading and writing.
%
The implementation of \texttt{ByteArrayInputStream} maintains an
internal buffer containing bytes that can be read from the stream, and
maintains a counter that keeps track of the next byte to be supplied
by the read method.
%
We expected the \texttt{read} procedure to run in linear time given
that its function intuitively takes time proportional to the number of
bytes being stored in the buffer. 
%
We also verified that the class's \texttt{skip} method, which skips
reading bytes from the input stream, also satisfies a linear bound
that we derived.

\paragraph{CryptoPermission}
\texttt{CryptoPermission} implements permission checks that check
whether an application or applet can access sensitive system resources
or run sensitive cryptographic algorithms.
%
We derived a linear bound for a critical method of
\texttt{CryptoPermission} by a manual inspection of the program and
documentation.

\paragraph{HashMap}
%
\texttt{HashMap} provides an implementation of a hash map.
%
Based on descriptions of standard operations supported by hash maps,
we derived an expected linear bound for the \texttt{lookup} method.
%
Furthermore, to evaluate the ability of \sys to find counter-examples
to specified bounds, we analyzed \texttt{lookup} to determine if it
satisfied constant bounds.
%
For each constant bound, \sys found inputs to \texttt{get} that
caused it to not satisfy the bound.
%
Similarly, we derived an expected bound for the \texttt{writeObject}
method that was linear, because \texttt{writeObject} saves the state
of the HashMap instance to a stream for serialization.
%
We used \sys to determine both that \texttt{writeObject} satisfies its
expected linear bound, and to synthesize inputs on which
\texttt{writeObject} does not satisfy various constant bounds.

\paragraph{PriorityQueue}
\texttt{PriorityQueue} implements a priority-queue datatype, which it
represents internally as a balanced binary heap.
%
\texttt{PriorityQueue} provides a method \texttt{peek} that a client
can invoke to retrieve the head element in the queue.
%
We derived an expected constant bound for \texttt{peek} based on our
understanding of the complexity of the operation.
%
\BH{edited up to here}

 (used to retrieve the head of the queue) runs in
constant time which is expected behavior while we obtained a violation
model for the \texttt{contains} ,\texttt{remove} and \texttt{grow}
methods which were then verified by setting a linear bound (e.g. $100n
+ 500$) indicating that the procedures do run as expected in worst
case linear time.

\paragraph{Rectangle}
We have also chosen to verify a few benchmarks from the Java Abstract
Window Toolkit, specifically the implementation of \texttt{Rectangle},
which contains functions for drawing rectangles and other utilities
for clipping points and lines in the two dimensional co-ordinate
space.  We once again proceeded and set our initial time complexity
bound to a linear one as indicated below in the table and proceeded to
see if the \texttt{setRect} and the \texttt{clip} functions satisfied
the bound.  We find that \sys returned a proof tree to us in about
half a minute indicating that their execution is valid within the
given time bounds.

\paragraph{RegularEnumSet}
To test our implementation against constant bounds, we set a constant
bound as mentioned in the table in the benchmark
\texttt{RegularEnumSet} for the function \texttt{remove} (which removes the
last element in the set) and found that this execution path was
verified in constant time as was evident from the proof tree.

\paragraph{Vector}
The benchmark \texttt{Vector} tests if the operations involving Java
vectors do indeed adhere to their linear time complexity bounds. We
set an initial linear bound and find that \sys verifies the execution
path of the functions, \texttt{removeRange} and
\texttt{copyInto} and found that we were able to verify the execution
of these functions in around twenty seconds while still not consuming
a large amount of memory. \newline

We note that in almost all cases, \sys verifies that the program
satisfies a given bound or indicates a violation in at most a few
minutes. This is reasonable given our interpolation approach where the
cost of the abstract post computation in predicates is avoided with
increasing predicate sizes.

% Results
\subsection{Results}
\label{sec:results}
\begin{table*}[t]
  \begin{tabular}{|l|r|r|r||r||r|r|}
    \hline
    \multicolumn{4}{|c||}{Program Structure} & \multicolumn{1}{c||}{Bound Structure} & \multicolumn{2}{c|}{Result} \\
    \hline
    \multicolumn{1}{|c|}{Name} & \multicolumn{1}{c|}{LOC} & \multicolumn{1}{c|}{No. of Loops} & \multicolumn{1}{c||}{Max. Nesting} & \multicolumn{1}{c||}{Bound} & \multicolumn{1}{c|}{Time} & \multicolumn{1}{c|}{Memory (MB)}\\
    \hline
    Arrays \textbf{::} mergesort & 4234 & 105 & 2 & $300n^2 + 400n$ & 11m 42s & 17 \\
    \hline
    BitSieve \textbf{::} retrieve & 234 & 6 & 2 & $5n^2 + 100$ & 0.9s & 38.2 \\
    \hline
    BitSet \textbf{::} flip & 995 & 24 & 2 & $100n^2 + 20n$ & 1m 15s & 20.8 \\ 
    \hline
    BitSet \textbf{::} set & 995 & 24 & 2 & $100n^2 + 20n$ & 1m 42s & 18.4 \\
    \hline
    ByteArrayInputStream \textbf{::} read & 299 & 2 & 1 & $100n + 200$ & 1.3s & 11.5 \\
    \hline
    ByteArrayInputStream \textbf{::} skip & 299 & 2 & 1 & $100n + 200$ & 1.2s & 14.1 \\ 
    \hline
    CryptoPermission \textbf{::} implies & 573 & 1 & 1 & $80n + 200$ & 3m 54s & 74.8 \\
    \hline
    HashMap \textbf{::} get & 1091 & 23 & 2 & $30n$ & 0.9s & 13.6 \\
    \hline
    HashMap \textbf{::} writeObject & 1091 & 23 & 2 & $200n + 400$ & 1.1s & 16.3 \\
    \hline
    PriorityQueue \textbf{::} grow & 763 & 10 & 1 & $100n + 500$ & 1.4s & 22.1 \\
    \hline
    PriorityQueue \textbf{::} contains & 763 & 10 & 1 & $100n + 500$ & 1.2s & 16.8 \\
    \hline
    PriorityQueue \textbf{::} remove & 763 & 10 & 1 & $100n + 500$ & 21.1s & 20.9 \\
    \hline
    PriorityQueue \textbf{::} peek & 763 & 10 & 1 & 100 (constant) & 0.8s & 14.1 \\ 
    \hline
    Rectangle \textbf{::} setRect & 1235 & - & - & $200n + 20$ & 1.9s & 22.2 \\ 
    \hline
    Rectangle \textbf{::} clip & 1235 & - & - & $200n + 20$ & 0.8s & 14.9 \\
    \hline
    RegularEnumSet \textbf{::} remove & 324 & - & - & 1000 (constant) & 0.9s & 14.9 \\
    \hline
    Vector \textbf{::} removeRange & 1203 & 7 & 1 & $50n$ & 21.1s & 19.5 \\
    \hline
    Vector \textbf{::} copyInto & 1203 & 7 & 1 & $30n$ & 1.1s & 15.5 \\
    \hline
  \end{tabular}
  \caption{Experimental results using \sys: `Name' represents the function's execution 
time in the benchmark we want to verify against a given Bound, `Bound' represents are bound formula 
we want to verify against, `LOC' is the number of lines of pre-processed code, 'Number of Loops' represents the total 
number of loops in the benchmark along with the 
max. nesting depth (`-' indicates the absence of a loop). 
`Time Taken' indicates the time taken to verify the 
given execution path, `Memory Used' refers to the consumption of 
memory during the verification process.}
\end{table*}

The results, obtained on a Lenovo ThinkPad L421 laptop with a 2.4 * 4
GHz Intel Core i5 processor and 4GB RAM, are summarised in Table 1
shown below.

We present the time taken to verify if a given abstraction in the
benchmark executed within the specified bound as mentioned in the
table. In each of the cases, we insert a dummy marker (user-defined)
in the benchmark to verify if the program run up until that point
satisfies the given bound.

The table shows the amount of time that \sys takes to construct
interpolants for path refinement to prove program safety. In case it
is unable to construct a proof of safety, it returns a violation model
indicating that the bound has been violated. We present here
performance data for only program paths that are found to be safe.

The numbers in the table refer to benchmarks where error cases
(statements involving unsupported instruction semantics or missing
dependencies) are commented out for our verification purposes. We now
briefly discuss some of the results that we obtained on running the
experiments mentioned above.

We also present the memory used during this verification process which doesn't exceed a few megabytes 
indicating that the resource utilization of \sys isn't very large allowing for practical usage 
on larger code bases.

Since we use the notion of application edges to model composition of abstractions, we find that we can reason 
more effectively about interprocedural analysis and loops providing us with stronger safety guarantees of 
the running time of the program. We did notice that in some benchmarks in the Java Platform Library, \sys 
never converged to prove program safety (which is reasonable given that verifying program safety is an 
undecidable problem) when it contained deeply nested loops resulting in our unwind procedure being executed 
for an indefinite amount of time. We find that removing application edges from our experiments results in us obtaining spurious results in the case where programs run in polynomial time.

We also note that in all cases, we only specify a loose bound on the execution time of an abstraction in 
the benchmark and check to see if it satisfies this bound. We could in fact do a binary search on the space of 
the coefficients appearing in the bound formula in order to derive tighter bounds on the execution time, 
but currently this hasn't been implemented in our tool.

While we have tested our framework on benchmarks of moderate sizes, we believe that scaling to larger code 
bases should not present much of a challenge given that encoding additional instruction semantics is fairly 
straightforward in \sys. 

In a few benchmarks containing nested loops (depth greater than four),
we also observed that carrying out our refine operation on program
paths or arbitrary lengths turns out to be expensive and hence we use
an approach similar to \texttt{forced covering} presented by McMillan
et.al \cite{mcmillan06} in order to speedup our converging of program
safety indicating that if the program doesn't have bugs while
executing in a small number of steps, the program path is in fact safe
w.r.t to the specified bound.

Another point to observe is that we only handle the cases for 32-bit machine words. We could in fact vary the 
bit-width of our distinguished counter variable and generalize our technique to machine words of arbitrary 
bit-width which could potentially result in larger verification times, but we leave this as future directions for this work. 

Although we have tested our tool on a restricted set of benchmarks, based on the results we have obtained here,
we see no reason why this technique can't be used as an effective static analysis to verify time complexity bounds 
for larger code bases.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "p"
%%% End: 

