package samples.quadratic;

import toy_benchmark.ErrorFunction;

public class OnmSimpleSolution {

	public static void main(String args[]) {

		OnmSimpleSolution onmsp = new OnmSimpleSolution();
		System.out.println(onmsp.uniquePaths(5, 3));

	}

	public int uniquePaths(int m, int n) {
		int[] first = new int[m];
		int[] second = new int[m];
		for (int i = 0; i < m; i++) {
			first[i] = 1;
		}
		for (int i = 1; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (j == 0) {
					second[j] = 1;
				} else {
					second[j] = second[j - 1] + first[j];
				}
			}
			first = second;
		}
		ErrorFunction.Error();
		return first[m - 1];
	}

}
