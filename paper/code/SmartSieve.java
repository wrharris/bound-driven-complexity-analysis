public static int[] SmartSieve(int MAX, int k) {
  boolean[] v = new boolean[MAX];
  int[] sp = new int[MAX];    
  for (int i = 2; i < MAX; i += 2)    
    sp[i] = 2; 
  for (int i = 3; i < MAX; i += 2) {
    if (!v[i]) {
      sp[i] = i;
      for (int j = i; j * i < MAX; j += 2) {
        if (!v[j * i]) {
          v[j * i] = true;
          sp[j * i] = i; } } } } 
  int[] ans = new int[sp.length];
  int j = 0;
  while(k > 1) {
    ans[j++] = sp[k];
    k /= sp[k]; }
  return ans; }
