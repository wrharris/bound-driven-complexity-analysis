package samples.quadratic;

import toy_benchmark.ErrorFunction;

public class OnmConciseVersion {

	public static void main(String[] args) {

		OnmConciseVersion ocv = new OnmConciseVersion();
		System.out.println(ocv.uniquePaths(6, 4));
	}

	public int uniquePaths(int m, int n) {

		int[][] dp = new int[m][n];

		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				dp[i][j] = i == 0 || j == 0 ? 1 : dp[i - 1][j] + dp[i][j - 1];

		ErrorFunction.Error();
		return dp[m - 1][n - 1];
	}

}
