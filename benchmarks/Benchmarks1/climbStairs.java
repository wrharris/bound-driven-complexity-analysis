//3. O(n) - very simple starts off with count and increments
public int climbStairs(int n) {
    if(n==1||n==0)
        return n;
    int count1=1;
    int count2=1;
    for(int i=2; i<=n; i++){
        int temp = count2;
        count2 = temp+count1;
        count1 = temp;
    }
    return count2;
}